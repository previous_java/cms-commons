package com.xyz.cmsWeb.route;

import com.gdatacloud.cms.controller.front.ArticleController;
import com.gdatacloud.cms.controller.front.ColumnController;
import com.gdatacloud.cms.controller.front.IndexController;
import com.gdatacloud.cms.core.JMainConst;
import com.jfinal.config.Routes;

public class DRoute extends Routes {

	@Override
	public void config() {
		add("/", IndexController.class, JMainConst.DEFAULT_TPL_PATH);
		add("/article", ArticleController.class, JMainConst.DEFAULT_TPL_PATH + "/modules/articles");
		add("/column", ColumnController.class, JMainConst.DEFAULT_TPL_PATH);
	}
	
}
