package com.xyz.cmsWeb.route;

import com.gdatacloud.cms.controller.manage.base.AdminActController;
import com.gdatacloud.cms.controller.manage.base.AdminArticleController;
import com.gdatacloud.cms.controller.manage.base.AdminColumnController;
import com.gdatacloud.cms.controller.manage.base.AdminConfigOption;
import com.gdatacloud.cms.controller.manage.base.AdminController;
import com.gdatacloud.cms.controller.manage.base.AdminDictionaryController;
import com.gdatacloud.cms.controller.manage.base.AdminModelController;
import com.gdatacloud.cms.controller.manage.base.AdminRelationLinksController;
import com.gdatacloud.cms.controller.manage.base.AdminUploadFileController;
import com.gdatacloud.cms.controller.manage.rbac.RoleController;
import com.gdatacloud.cms.controller.manage.rbac.UserController;
import com.gdatacloud.cms.controller.manage.weixin.publicno.AutoReplyController;
import com.gdatacloud.cms.controller.manage.weixin.publicno.PublicNOController;

import com.jfinal.config.Routes;

public class AdminRoute extends Routes{

	@Override
	public void config() {
		// TODO Auto-generated method stub
		add("/admin", AdminController.class,"/WEB-INF/content/manage");
		add("/admin/column",AdminColumnController.class,"/WEB-INF/content/manage");
		add("/admin/act",AdminActController.class);
		add("/admin/article",AdminArticleController.class,"/WEB-INF/content/manage");
		add("/admin/uploadfile",AdminUploadFileController.class,"/WEB-INF/content/manage");
		add("/admin/option",AdminConfigOption.class,"/WEB-INF/content/manage");
		add("/admin/dictionary",AdminDictionaryController.class,"/WEB-INF/content/manage");
		//模板管理
		add("/admin/model",AdminModelController.class,"/WEB-INF/content/manage/module");
		//公众号管理
		add("/admin/publicNo",PublicNOController.class,"/WEB-INF/content/manage/publicno");
		add("/admin/autoReply",AutoReplyController.class,"/WEB-INF/content/manage/reply");
		//权限管理
		add("/admin/user", UserController.class, "/WEB-INF/content/manage/rbac");
		add("/admin/role", RoleController.class, "/WEB-INF/content/manage/rbac");
		//友情链接管理
		add("/admin/relation", AdminRelationLinksController.class,"/WEB-INF/content/manage/module");
	}
}