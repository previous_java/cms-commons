package com.xyz.cmsWeb.route;

import com.gdatacloud.cms.controller.front.ArticleController;
import com.gdatacloud.cms.controller.front.ColumnController;
import com.gdatacloud.cms.controller.front.IndexController;
import com.gdatacloud.cms.controller.manage.dict.RegionController;
import com.gdatacloud.cms.core.JMainConst;
import com.jfinal.config.Routes;

public class ConfigRoute extends Routes {

	@Override
	public void config() {
		add("region", RegionController.class);
	}
	
}
