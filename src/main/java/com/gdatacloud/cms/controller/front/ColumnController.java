package com.gdatacloud.cms.controller.front;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class ColumnController extends Controller {

	/**
	 * 前台栏目对应页面
	 */
	public void page() {
		Integer columnId = getParaToInt("c");
		if (columnId == null) {
			render("modules/articles/page.html");
			return;
		}
		Record column = Db.findById("g_common_column", columnId);
		// 接收栏目编号或者编码
		setAttr("column", column);
		// 根据编号或者编码取出对应的模板文件，如果没有另外指定，那么使用默认的
		render(column.getStr("tpl_path"));
	}
	
	/**
	 * 搜索页面
	 */
	public void search() {
		render("modules/articles/search.html");
	}
}
