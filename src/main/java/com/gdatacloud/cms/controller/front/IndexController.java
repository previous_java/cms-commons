package com.gdatacloud.cms.controller.front;

import com.gdatacloud.cms.util.JMStaticiseUtil;
import com.jfinal.core.Controller;
import com.jfinal.plugin.ehcache.CacheKit;

/**
 * 首页控制器
 * @author zzp
 *
 */
public class IndexController extends Controller {

	/**
	 * 进入首页
	 */
	public void index() {
		/*CacheKit.put("ac", "1", "1");
		String t = CacheKit.get("ac", "1");
		System.out.println(t);
		setAttr("columnId", 1);
		// 这个地方其实需要传入一个模板文件夹
		new JMStaticiseUtil().staticize(getRequest());*/
		setAttr("pageNumber", getPara(0));
		render("index.html");
	}
	/**
	 * 根据不同模型，转发到不同页面。用于自定义页面
	 */
	public void dispatcher() {
		String baseTplPath = "/WEB-INF/templates/default/modules/";
		// 首先获取模型名称，根据模型名称来输出对应的模板文件
		String moduleName = getPara(0, "articles");
		// 其次判断要请求的是模板中的哪个页面，页面中的数据让其通过标签或js来取
		String htmlName = getPara(1, "list");
		setAttr("basePath", "/WEB-INF/content/");
		render(baseTplPath + moduleName + "/" + htmlName + ".html");
	}
	
	public void console() {
		renderText("控制台");
	}
}
