package com.gdatacloud.cms.controller.front;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.gdatacloud.cms.model.articles.Articles;
import com.gdatacloud.cms.model.articles.ArticlesFile;
import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 首页控制器
 * @author zzp
 *
 */
public class ArticleController extends Controller {

	/**
	 * 点击顶部导航跳转到对应页面
	 */
	
	
	public void page() {
		// 接收顶部导航菜单的id，根据此id查询其二级菜单和其下文章列表
		Long columnId = getParaToLong("columnId");
		setAttr("columnId", columnId);
		setAttr("pageNumber", getPara(0));
		render("page.html");
	}
	/**
	 * 展示详情页
	 */
	public void show() {
		/*Integer contentId = getParaToInt(0); // 这个需要拦截，使其不能为空，否则404
		// 访问量更新语句
		String updateVisitCount = "update g_articles set visit_count=visit_count+1 where id=?";
		Db.update(updateVisitCount, contentId);*/
		render("article_show.html");
	}
	/**
	 * 下载
	 */
	public void download() {
		renderFile(new File(PathKit.getWebRootPath() + "/" + getPara("path")));
	}
	
	
	/**
	 * 验证用户是否登录
	 */
	public void checkUser(){
		if(getSessionAttr("userId") == null){
			setReturnCode(LOGIN);
		}else{
			setReturnCode(SUCCESS);
		}
		renderJson(resp);
	}
}
