package com.gdatacloud.cms.controller.front;

import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 首页控制器
 * @author zzp
 *
 */
public class ContentController extends Controller {

	/**
	 * 点击顶部导航跳转到对应页面
	 */
	public void list() {
		// 接收顶部导航菜单的id，根据此id查询其二级菜单和其下文章列表
		Long categoryId = getParaToLong("categoryId");
		List<Record> leftMenuList = Db.find(
				"select * from jpress_taxonomy where parent_id=?"
				, categoryId);
		List<Record> articleList = Db.find(
				"select * from jpress_taxonomy where parent_id=?"
				);
	}
	
	public void show() {
		
	}
	
}
