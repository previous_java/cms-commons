package com.gdatacloud.cms.controller.manage.base;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.gdatacloud.cms.model.models.ModelsInfo;
import com.gdatacloud.cms.util.ReadTxt;
import com.gdatacloud.cms.util.WriteTxt;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

public class AdminModelController extends Controller {
	
	public void index() {
	}

	public void model() {
		render("model.html");
	}

	public void modelList() {
		String modelName = getPara("name");
		Page<ModelsInfo> modelsInfos = ModelsInfo.dao.getModelList(getParaToInt("curr", 1), 10, modelName);
		setReturnCode(SUCCESS);
		setReturnContent(modelsInfos);
		renderJson(resp);
	} // 添加修改model 
	
	public void addOrupdateInfo() {
		ModelsInfo info = getModel(ModelsInfo.class, "model");
		String rootPath = getRequest().getRealPath("/WEB-INF/") + "templates" + "/" + "default" + "/";
		// 记录当前文件的绝对路径
		String modelPath = info.getModelPath();
		//File file = new File(rootPath + modelPath);
		String content = getPara("content");
		WriteTxt.writeFile(rootPath, rootPath + modelPath, content);
		if (getParaToLong("acId") == null) {
			ModelsInfo modelsInfo = ModelsInfo.dao.getInfoByName(info.getName());
			if (modelsInfo != null) {
				setReturnCode(ERROR);
				renderJson(resp);
				return;
			}
			info.setStatus(1);
			info.setCreateTime(new Date());
			info.save();
		} else {
			ModelsInfo modelsInfo = ModelsInfo.dao.getInfoByName(info.getName());
			if (modelsInfo != null ) {
				if(modelsInfo.getId() == getParaToLong("acId")){
					info.setId(getParaToLong("acId"));
					info.setUpdateTime(new Date());
					info.update();
				}else{
					setReturnCode(ERROR);
					renderJson(resp);
					return;
				}
			}else{
				info.setId(getParaToLong("acId"));
				info.setUpdateTime(new Date());
				info.update();
			}
			
			
		}
		setReturnCode(SUCCESS);
		renderJson(resp);
	}

	public void getModelById() {
		Map<String, Object> map = new HashMap<String, Object>();
		Long modelId = getParaToLong("acId");
		ModelsInfo info = ModelsInfo.dao.findById(modelId);
		String modelPath = info.getModelPath();
		// String rootPath=Class.class.getClass().getResource("/").getPath();
		// 读取文件
		String rootPath = getRequest().getRealPath("/WEB-INF/") + "templates" + "/" + "default" + "/";//
		// 记录当前文件的绝对路径
		File file = new File(rootPath + modelPath);
		if (file.exists()) {
			String modelContent = ReadTxt.readFileGetRows(file);
			map.put("modelContent", modelContent);
		} else {
			map.put("modelContent", "");
		}
		map.put("info", info);
		setReturnCode(SUCCESS);
		setReturnContent(map);
		renderJson(resp);
	}
	
	// 删除model
	public void delModel() {
		Long modelId = getParaToLong("acId");
		if (modelId != null) {
			ModelsInfo info = ModelsInfo.dao.findById(modelId);
			info.setStatus(2);// 删除 setReturnCode(SUCCESS); }else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
}