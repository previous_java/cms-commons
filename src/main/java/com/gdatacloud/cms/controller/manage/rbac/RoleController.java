package com.gdatacloud.cms.controller.manage.rbac;


import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import com.gdatacloud.cms.model.rbac.RbacMenu;
import com.gdatacloud.cms.model.rbac.RbacRole;
import com.gdatacloud.cms.model.rbac.RbacRoleMenu;
import com.gdatacloud.cms.model.user.UserLogin;
import com.gdatacloud.cms.util.weixin.wex.util.VarValid;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
/**
 * 角色管理
 * @author xyz
 *
 */
public class RoleController extends Controller {
	/**
	 * 前往角色管理页面
	 */
	public void index() {
		//setSessionAttr("userId", 1);
		render("role.html");
	}
	/**
	 * 条件查询获取角色列表
	 */
	public void list() {
		RbacRole role=getModel(RbacRole.class,"role");
		Page<?> page = null;
		page = RbacRole.dao.list(getParaToInt("curr", 1), 10, role);
		setReturnCode(SUCCESS);
		setReturnContent(page);
		renderJson(resp);
	}
	/**
	 * 获取角色信息
	 */
	@Clear
	public void getInfo(){
		Long id = getParaToLong("id");
		RbacRole role = RbacRole.dao.findById(id);
		if(role != null){
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		setReturnContent(role);
		renderJson(resp);
	}
	/**
	 * 新增、修改角色信息
	 * @throws UnknownHostException 
	 */
	public void update() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		RbacRole role = getModel(RbacRole.class,"role");
		//新增
		if(!VarValid.isValidOfLong(role.getLong("id"))){
			if(VarValid.isValidOfString(role.getStr("name")) && VarValid.isValidOfString(role.getStr("des"))){
				role.set("create_time", new Date());
				if(role.save()){
					setReturnCode(SUCCESS);
					//写入系统日志
					//SysLogs.dao.logs(SysLogs.TYPE_ADD,"新增角色信息", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());

				}else{
					setReturnCode(ERROR);
				}
			}
		}else{//更新
			if(VarValid.isValidOfString(role.getStr("name")) && VarValid.isValidOfString(role.getStr("des"))){
				role.set("update_time", new Date());
				if(role.update()){
					setReturnCode(SUCCESS);
					//写入系统日志
					//SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"修改角色信息", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());

				}else{
					setReturnCode(ERROR);
				}
			}
		}
		renderJson(resp); 
	}
	/**
	 * 删除角色信息
	 * @throws UnknownHostException 
	 */
	public void del() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		Long id = getParaToLong("id");
		RbacRole role = RbacRole.dao.findById(id);
		role.set("status", 2).set("update_time", new Date());
		
		if(role.update()){
			setReturnCode(SUCCESS);
			//写入系统日志
			//SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"删除角色信息", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());

		}else{
			setReturnCode(ERROR);
		}
		setReturnContent(role);
		renderJson(resp);
	}
	/**
	 * 获取菜单列表
	 */
	@Clear
	public void getMenuList() {
		List<?> dataList = Db.find("select * from g_rbac_menu where status!=0 order by sort asc");
		setReturnCode(SUCCESS);
		setReturnContent(dataList);
		renderJson(resp);
	}
	/**
	 * 获取角色所拥有的权限
	 */
	@Clear
	public void getRoleMenu(){
		Long roleid = getParaToLong("roleid");
		if(!VarValid.isValidOfLong(roleid)){
			setReturnCode(ERROR);
		}else{
			List<RbacRoleMenu> list = RbacRoleMenu.dao.find("select * from  g_rbac_role_menu where roleid=? ",roleid);
			setReturnCode(SUCCESS);
			setReturnContent(list);
		}
		renderJson(resp);
	}
	/**
	 * 获取角色所拥有的权限
	 */
	@Clear
	public void getRoleMenu2(){
		Long userid = getParaToLong("userid");
		UserLogin user =UserLogin.dao.findById(userid);
		Integer roleid	= 0;	
		if(user != null){
			roleid = user.getRoleId();
		}
		if(!VarValid.isValidOfInteger(roleid)){
			setReturnCode(ERROR);
		}else{
			List<RbacRoleMenu> list = RbacRoleMenu.dao.find("select * from  g_rbac_role_menu where roleid=? ",roleid);
			setReturnCode(SUCCESS);
			setReturnContent(list);
		}
		renderJson(resp);
	}
	/**
	 * 修改角色权限
	 * @throws UnknownHostException 
	 */
	public void addRoleMenu() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		Long roleid = getParaToLong("roleid");
 		Long menuid = getParaToLong("menuid");
 		RbacRoleMenu rbacRoleMenu = RbacRoleMenu.dao.findFirst("select * from g_rbac_role_menu where roleid=? and menuid=?",roleid,menuid);
 		if(rbacRoleMenu == null){//勾选操作
 			rbacRoleMenu = new RbacRoleMenu();
 			rbacRoleMenu.set("roleid", roleid).set("menuid", menuid).set("create_time", new Date());
 			if(rbacRoleMenu.save()){
 				//查看上级菜单
 				RbacMenu rbacMenu = RbacMenu.dao.findFirst("select * from g_rbac_menu where id=?",menuid);
 				if(rbacMenu != null){
 					//先判断是否有上级菜单
 					RbacMenu rbacMenu1 = RbacMenu.dao.findFirst("select * from g_rbac_menu where id=?",rbacMenu.getLong("pid"));
 					if(rbacMenu1 != null){
 						RbacRoleMenu rbacRoleMenu1 = RbacRoleMenu.dao.findFirst("select * from g_rbac_role_menu where roleid=? and menuid=?",roleid,rbacMenu1.getLong("id"));
 	 					if(rbacRoleMenu1==null){//如果上级菜单未勾选，则关联勾选父菜单
 	 						rbacRoleMenu1 = new RbacRoleMenu();
 	 						rbacRoleMenu1.set("roleid", roleid).set("menuid", rbacMenu1.getLong("id")).set("create_time", new Date()).save();//关联勾选父菜单
 	 						//判断是否有上上级菜单
 	 		 				RbacMenu rbacMenu2 = RbacMenu.dao.findFirst("select * from g_rbac_menu where id=?",rbacMenu1.getLong("pid"));
 	 		 				if(rbacMenu2 != null){
 	 		 					RbacRoleMenu rbacRoleMenu2 = RbacRoleMenu.dao.findFirst("select * from g_rbac_role_menu where roleid=? and menuid=?",roleid,rbacMenu2.getLong("id"));
 	 	 	 					if(rbacRoleMenu2==null){//如果上级菜单未勾选，则关联勾选父菜单
 	 	 	 					rbacRoleMenu2 = new RbacRoleMenu();
 	 	 	 					rbacRoleMenu2.set("roleid", roleid).set("menuid", rbacMenu2.getLong("id")).set("create_time", new Date()).save();//关联勾选父菜单
	 	 		 				}
	 	 					}
	 					}
	 				}
 					//判断下级菜单      
	 				List<RbacMenu>  list1 = RbacMenu.dao.find("select * from g_rbac_menu where pid=?",rbacMenu.getLong("id"));
 					if(list1.size() != 0){//存在下级菜单
 						for (RbacMenu rbacMenu2 : list1) {
 							RbacRoleMenu  roles1 = new RbacRoleMenu();
 							roles1.set("roleid", roleid).set("menuid", rbacMenu2.getLong("id")).set("create_time", new Date()).save();//关联勾选下级菜单
 							List<RbacMenu>  list2 = RbacMenu.dao.find("select * from g_rbac_menu where pid=?",rbacMenu2.getLong("id"));
 							if(list2.size() != 0 ){//如果存在下下级菜单
 								for (RbacMenu rbacMenu3 : list2) {
 									RbacRoleMenu  roles2 = new RbacRoleMenu();
 		 							roles2.set("roleid", roleid).set("menuid", rbacMenu3.getLong("id")).set("create_time", new Date()).save();//关联勾选下级菜单
								}
 							}
 						}
 					}
 				}
 				setReturnCode(SUCCESS);
 				//写入系统日志
 				//SysLogs.dao.logs(SysLogs.TYPE_ADD,"新增角色权限", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());

 			}else{
 				setReturnCode(ERROR);
 			}
 		}else{//取消勾选操作
 			if(rbacRoleMenu.delete()){
 				//判断下级菜单      
 				List<RbacMenu>  list1 = RbacMenu.dao.find("select * from g_rbac_menu where pid=?",menuid);
					if(list1.size() != 0){//存在下级菜单
						for (RbacMenu rbacMenu2 : list1) {
							Long menuid2 = rbacMenu2.getId();
							RbacRoleMenu rbacRoleMenu1 =RbacRoleMenu.dao.findFirst("select * from g_rbac_role_menu where roleid=? and menuid=?",roleid,menuid2);
							if(rbacRoleMenu1 != null){//如果下级菜单被勾选，则取消下级勾选
								rbacRoleMenu1.delete();
							}
							List<RbacMenu>  list2 = RbacMenu.dao.find("select * from g_rbac_menu where pid=?",menuid2);
							if(list2.size() != 0){
								for (RbacMenu rbacMenu3 : list2) {
									RbacRoleMenu rbacRoleMenu2 =RbacRoleMenu.dao.findFirst("select * from g_rbac_role_menu where roleid=? and menuid=?",roleid,rbacMenu3.getId());
									if(rbacRoleMenu2 != null){//如果下下级菜单被勾选，则取消下级勾选
										rbacRoleMenu2.delete();
									}
								}
							}
						}
					}
 				setReturnCode(SUCCESS);
 				//写入系统日志
 				//SysLogs.dao.logs(SysLogs.TYPE_REMOVE,"删除角色权限", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());

 			}else{
 				setReturnCode(ERROR);
 			}
 		}
		renderJson(resp);
	}
}