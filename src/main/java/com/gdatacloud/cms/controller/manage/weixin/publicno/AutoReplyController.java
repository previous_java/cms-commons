package com.gdatacloud.cms.controller.manage.weixin.publicno;




import java.net.UnknownHostException;

import com.gdatacloud.cms.model.user.UserLogin;
import com.gdatacloud.cms.model.weixin.WeixinAutomaticResponse;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.xyz.util.IpUtil;
@Clear
public class AutoReplyController extends Controller{

	/**
	 * 前往自动回复管理界面
	 */
	public void toAutoReply(){
		render("autoReply.html");
	}
	/**
	 * 获取分页数据
	 */
	public void autoReply(){
		String key = getPara("name");
		String sql="";
		if(key==null || "".equals(key)){
			sql="FROM g_weixin_automatic_response";
		}else{
			sql="FROM g_weixin_automatic_response where key_word = '"+key+"'";
		}
		Page<?> datapage=Db.paginate(getParaToInt("pageNumber", 1), 10, "SELECT *",sql);
		
		setReturnCode(SUCCESS);
		setReturnContent(datapage);
		renderJson(resp);
	}
	
	/**
	 * 保存或修改字段回复内容
	 * @throws UnknownHostException 
	 */
	public void saveAutoReply() throws UnknownHostException{
		/*获取当前登录用户信息*/
		Long userId = getSessionAttr("userId");
		Long replyId= getParaToLong("replyId");
		WeixinAutomaticResponse publicnoAutoreply = getModel(WeixinAutomaticResponse.class);
		if(replyId==null){	//保存
			WeixinAutomaticResponse autoreply=WeixinAutomaticResponse.dao.findByKeyWord(publicnoAutoreply.getKeyWord());
			if(autoreply!=null){	//若存在,返回重复
				setReturnCode(ERROR);
				renderJson(resp);
				return;
			}else{
				publicnoAutoreply.save();	
				UserLogin login = UserLogin.dao.findById(userId);
				/*if(login !=null){
				//写入系统日志
				SysLogs.dao.logs(SysLogs.TYPE_ADD,"新增微信关键字自动回复", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
				}*/
			}
			}else{
		publicnoAutoreply.setId(replyId);
		publicnoAutoreply.update();
		UserLogin login = UserLogin.dao.findById(userId);
		/*if(login !=null){
		//写入系统日志
		SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"修改微信关键字自动回复", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
		}*/
		setReturnContent(publicnoAutoreply);
		}
		setReturnCode(SUCCESS);
		renderJson(resp);
		}
		
	/**
	 *查询当前回复内容
	 */
	public void findAutoReply(){
		Long replyId= getParaToLong("replyId");
		WeixinAutomaticResponse autoreply=WeixinAutomaticResponse.dao.findById(replyId);
		setReturnCode(SUCCESS);
		setReturnContent(autoreply);
		renderJson(resp);
	}
	/**
	 * 删除自动回复
	 * @throws UnknownHostException 
	 */
	public void delAutoReply() throws UnknownHostException{
		Long replyId= getParaToLong("replyId");
		 WeixinAutomaticResponse.dao.deleteById(replyId);
		 Long userId = getSessionAttr("userId");
		 UserLogin login = UserLogin.dao.findById(userId);
			/*if(login !=null){
			//写入系统日志
			SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"删除微信关键字自动回复", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
			}*/
		 renderText("success");
	}
	/**
	 * 根据关键词查询自动回复
	 */
	public void findAutoReplyByKey(){
		//原获取List集合,被修改
		String key = getPara("key");
		WeixinAutomaticResponse autoreply=WeixinAutomaticResponse.dao.findByKeyWord(key);
		//setReturnContent(autoreply);
		renderJson(autoreply);
	}
}
