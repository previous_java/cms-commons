package com.gdatacloud.cms.controller.manage.weixin.publicno;


import java.io.File;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gdatacloud.cms.model.user.UserLogin;
import com.gdatacloud.cms.model.weixin.WeixinMenu;
import com.gdatacloud.cms.model.weixin.WeixinPublicInfo;
import com.gdatacloud.cms.util.weixin.main.MenuManager;
import com.gdatacloud.cms.util.weixin.wex.menu.Button;
import com.gdatacloud.cms.util.weixin.wex.menu.ComplexButton;
import com.gdatacloud.cms.util.weixin.wex.menu.Menu;
import com.gdatacloud.cms.util.weixin.wex.menu.ViewButton;
import com.gdatacloud.cms.util.weixin.wex.pojo.Token;
import com.gdatacloud.cms.util.weixin.wex.util.AdvancedUtil;
import com.gdatacloud.cms.util.weixin.wex.util.CommonUtil;
import com.gdatacloud.cms.util.weixin.wex.util.MenuUtil;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.upload.UploadFile;
import com.xyz.util.IpUtil;

import net.sf.json.JSONObject;
/**
 * 公众号
 * 
 *
 */
@Clear
public class PublicNOController extends Controller{
	private static Logger log = LoggerFactory.getLogger(MenuManager.class);
	
	/*------------------------------------------基本信息----------------------------------------*/
	/**
	 * 前往基本信息界面。以前方法没有动
	 */
	public void toBaseinformation(){
		WeixinPublicInfo accounts=WeixinPublicInfo.dao.findById(1); //暂定为1
		setAttr("accounts", accounts);
		render("baseInformation.html");
	}
	//上传公众号二维码
	public void getUpload() {
		UploadFile file1 = getFile("img_files"); 
		 File resouceFile =new File(getRequest().getRealPath("/upload")+"/image/qrcode");    
			//如果文件夹不存在则创建    
	       if(!resouceFile .exists()  && !resouceFile .isDirectory()){       
	    	   resouceFile.mkdirs();    
	       }  
		long  currentime = System.currentTimeMillis();
		if (file1!=null) {
			String realpath = getRequest().getRealPath("/upload")+"/image/qrcode";
			File file = file1.getFile();
			String fileName = file1.getFileName();	
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}
	    
		renderJson(resp);
	}
	//点保存按钮执行的方法
	public void updataInfo(){
		WeixinPublicInfo info =getModel(WeixinPublicInfo.class,"accounts");
		
			info.setId((long)1);
			info.setImg(getPara("m.img"));
			info.setUpdateTime(new Date());
			info.update();
			
			setReturnCode(SUCCESS);
			setReturnContent(info);
		renderJson(resp);
	}
	 
	/*------------------------------------------菜单管理----------------------------------------------*/
	public void toMenu(){
		
		render("menu.html");
	}
	/**
	 * 获取名称
	 */
	public void getname(){
		Long userId = getSessionAttr("userId");
		UserLogin login = UserLogin.dao.findById(userId);
		System.out.println(login);
		renderJson(login);
	}
	/**
	 * 获取主菜单
	 */
	public void getMainMenu(){
		
		List<WeixinMenu> mainMenu=WeixinMenu.dao.getMainMenu();
		System.out.println(mainMenu);
		renderJson(mainMenu);
	}
	
	/**
	 * 获取子菜单
	 */
	public void getKidMenu(){
		long mainId= getParaToLong("mainId");
		if(getParaToLong("mainId")==null){
			renderJson("error");
			return;
		}else{
		List<WeixinMenu> kidMenu=WeixinMenu.dao.getKidMenu(mainId);
		setReturnCode(SUCCESS);
		setReturnContent(kidMenu);
		System.out.println(resp);
		renderJson(resp);
		return;
		}
	}
	/**
	 * 保存主菜单
	 * @throws UnknownHostException 
	 */
	public void saveMianMenu() throws UnknownHostException{
	  long mainId=0;
	  Long userId = getSessionAttr("userId");
	 WeixinMenu menuModel=getModel(WeixinMenu.class);
	 if(getParaToLong("mainId")==0){
		 System.out.println("哈哈哈");
	 /*PublicnoMenu menu=PublicnoMenu.dao.findPublicnoMenu(menuModel.getOffcialAccountsId(), menuModel.getLocation());
	 if(menu==null){*/
	 menuModel.save();
	 	UserLogin login = UserLogin.dao.findById(userId);
		/*if(login !=null){
		//写入系统日志
		SysLogs.dao.logs(SysLogs.TYPE_ADD,"添加主菜单", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
		}*/
      mainId=menuModel.getId();
      
	/* }else{
		menuModel.setId(menu.getId());
		 menuModel.update();
		 mainId=menu.getId();
	 }*/
	 }else{
		 mainId=getParaToLong("mainId");
		 menuModel.setId(mainId);
		 menuModel.update();
		 UserLogin login = UserLogin.dao.findById(userId);
			/*if(login !=null){
			//写入系统日志
			SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"修改主菜单", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
			}*/
	 }
	 renderJson(mainId);
	}
	/**
	 * 保存子菜单
	 * @throws UnknownHostException 
	 */
	public void saveKidMemu() throws UnknownHostException{
		long kidId=0;
		Long userId = getSessionAttr("userId");
		WeixinMenu menuModel=getModel(WeixinMenu.class);
		System.out.println(getParaToLong("kidId"));
		System.out.println(menuModel.getPid());
		Long pid=menuModel.getPid();
		if(pid==null){
			renderJson("error");
			return ;
			
		}else{
			if(getParaToLong("kidId")==0){
				menuModel.save();
				UserLogin login = UserLogin.dao.findById(userId);
				/*if(login !=null){
				//写入系统日志
				SysLogs.dao.logs(SysLogs.TYPE_ADD,"添加子菜单", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
				}*/
				WeixinMenu menu=WeixinMenu.dao.findPublicnoMenu(pid, menuModel.getLocation());
				kidId=menu.getId();
				WeixinMenu mainmenu=WeixinMenu.dao.findById(pid);
				if(mainmenu.getUrl()!=null){
					mainmenu.setUrl("");
				}
			}else{
				kidId=getParaToLong("kidId");
				menuModel.setId(kidId);
				menuModel.update();
				UserLogin login = UserLogin.dao.findById(userId);
				/*if(login !=null){
				//写入系统日志
				SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"修改子菜单", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
				}*/
			}
			renderJson(kidId);
		}
	}
	/**
	 * 保存并发布菜单
	 * @throws UnknownHostException 
	 */
	public void saveAndRelease() throws UnknownHostException{
		WeixinPublicInfo accounts = WeixinPublicInfo.dao.findById(1);
		List<WeixinMenu> mainMenu=WeixinMenu.dao.getMainMenu();
		if(mainMenu==null){
			renderText("nomenu");
			return;
		}else{
			Menu menu = new Menu();
			Button	menubutton[] = new Button[mainMenu.size()]; 
			for (int i = 0; i < mainMenu.size(); i++) {
				Long mainId=mainMenu.get(i).getId();
				List<WeixinMenu> kidMenu=WeixinMenu.dao.getKidMenu(mainId);
				//若无子菜单
				if(kidMenu==null || kidMenu.size()==0){
					//当前按钮仅为viewbutton  以后若有clickbutton 将在此进行判断
					/*String mainUrl=mainMenu.get(i).getUrl();
					if(mainUrl==null || "".equals(mainUrl)){
						
					}*/
					ViewButton button= new ViewButton();
					button.setName(mainMenu.get(i).getName());
					button.setType("view");
					button.setUrl(mainMenu.get(i).getUrl());;
					menubutton[i]=button;
				}else{
					ComplexButton mainButton=new ComplexButton();
					Button kidMenuButton[]= new Button[kidMenu.size()];
					mainButton.setName(mainMenu.get(i).getName());
					/*	mainBtn1.setName("我的会议");
		mainBtn1.setSub_button(new Button[] { btn13, btn32 });*/
					for (int j = 0; j < kidMenu.size(); j++) {
						//当前子菜单按钮仅为viewbutton  以后若有clickbutton 将在此进行判断
						ViewButton kidbutton= new ViewButton();
						kidbutton.setName(kidMenu.get(j).getName());
						kidbutton.setType("view");
						kidbutton.setUrl(kidMenu.get(j).getUrl());
						kidMenuButton[j]=kidbutton;
					}
					mainButton.setSub_button(kidMenuButton);
					menubutton[i]=mainButton;
					
				}
			}			
			menu.setButton(menubutton);
			System.out.println(JSONObject.fromObject(menu).toString());
			// 第三方用户唯一凭证
			//String appId = "wxa6a51f9e8f6b84bd";
			String appId =accounts.getAppId();
			// 第三方用户唯一凭证密钥
			//String appSecret = "bb26cb1cc7960ae4f49fbb219f07ae5f";
			String appSecret= accounts.getAppSecret();
			// 调用接口获取凭证
			Token token = CommonUtil.getToken(appId, appSecret);
			if (null != token) {
				// 创建菜单
				boolean result = MenuUtil.createMenu(menu, token.getAccessToken());
				// 判断菜单创建结果
				if (result){
					log.info("菜单创建成功！");
					renderText("success");
					Long userId = getSessionAttr("userId");
					UserLogin login = UserLogin.dao.findById(userId);
					/*if(login !=null){
						//写入系统日志
						SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"保存并发布微信公众号菜单", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
					}*/
					return;
				}else{
					log.info("菜单创建失败！");
					renderText("error");
				}
		}
			
		}
	} 
	/**
	 * 删除主菜单及其自带单
	 * @throws UnknownHostException 
	 */
	public void delMainMenu() throws UnknownHostException{
	Long mainId=getParaToLong("mainId");
	if(getParaToLong("mainId")==null){
		renderText("error");
		return ;
	}else{
		List<WeixinMenu> kidMenu=WeixinMenu.dao.getKidMenu(mainId);
		if(kidMenu.size()>0){
			Db.update("delete from g_weixin_menu where pid=?", mainId);
			WeixinMenu.dao.delMainMenu(mainId);
		}else{
			WeixinMenu.dao.delMainMenu(mainId);
		}
		Long userId = getSessionAttr("userId");
		UserLogin login = UserLogin.dao.findById(userId);
	/*	if(login !=null){
			//写入系统日志
			SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"删除微信公众号主菜单及其自带单", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
		}*/
		renderText("success");
	}
	}
	/**
	 * 删除子菜单
	 * @throws UnknownHostException 
	 */
	public void delKidMenu() throws UnknownHostException{
		Long kidId=getParaToLong("kidId");
		if(getParaToLong("kidId")==null){
			renderText("error");
			return;
		}else{
			WeixinMenu.dao.deleteById(kidId);
			Long userId = getSessionAttr("userId");
			UserLogin login = UserLogin.dao.findById(userId);
			/*if(login !=null){
				//写入系统日志
				SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"删除微信公众号子菜单", login.getRoleId(), login.getStr("name"), login.getLong("id"), IpUtil.getLocalIp());
			}*/
			renderText("success");
		}
	}
	/**
	 * 点击菜单自动发送文本	
	 */
	public void sendText(){
		 String openId = getRequest().getAttribute("openId").toString(); 
		 WeixinPublicInfo publicInfo = WeixinPublicInfo.dao.findById(1);		//暂时为1
		// 根据openid和公众号标识判断用户是否存在
		 //发送文本信息
		  AdvancedUtil.sendTextMessage(openId,publicInfo.getOrigionId(),  "欢迎!!");
	}
	
	public void getbutton(){
		int buttonId=getParaToInt("buttonId");
		WeixinMenu menu= new WeixinMenu();
			if(buttonId!=0){
				menu=WeixinMenu .dao.findById(buttonId);
			}
			renderJson(menu);
	}
	
}
