package com.gdatacloud.cms.controller.manage.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gdatacloud.cms.model.rbac.RbacPersonMenu;
import com.gdatacloud.cms.model.rbac.RbacRoleMenu;
import com.gdatacloud.cms.model.user.UserLogin;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
/*import com.xyz.bean.Children;
import com.xyz.bean.MySort;
import com.xyz.drp.model.ac.AcAct;
import com.xyz.drp.validator.SaveActValidator;*/
import com.jfinal.plugin.ehcache.CacheKit;
 
/**
 * 操作控制器
 * @author xyz
 *
 */
public class AdminActController extends Controller {
	/**
	 * 转向菜单管理页面
	 */
	public void index() {
		render("menu.html");
	}
	/**
	 * 获取菜单列表
	 */
	public void getMenuList1() {
		List<?> dataList = Db.find("select * from g_ac_act");
		setReturnCode(SUCCESS);
		setReturnContent(dataList);
		renderJson(resp);
	}
	public void getPageList() {
		Long pId = getParaToLong("pId");
		Page<Record> dataList = Db.paginate(1, 15, "select *", "from g_ac_act where pid=?", pId);
		setReturnCode(SUCCESS);
		setReturnContent(dataList);
		renderJson(resp);
	}
	/**
	 * 获取菜单列表
	 */
	@Clear
	public void getMenuList() {
		// 这种应该是再定义一个拦截器，提示它登录
		//int roleId = CacheKit.get("ac", "roleId");
		Long userId = getSessionAttr("userId");
		//System.out.println(userId);
		UserLogin userLogin = UserLogin.dao.findById(userId);	//临时更改
		//该用户权限集合
		List<?> dataList = new ArrayList<Object>();
		Map<Long, Object> map = new HashMap<Long, Object>();
		
		//List<String> list = new ArrayList<String>();
	/*	if(userLogin.getRoleId()==1){//超级管理员
			 dataList = Db.find("select * from g_rbac_menu where status=1 order by sort");
			 	setReturnCode(SUCCESS);
				setReturnContent(dataList);
				renderJson(resp);
				return;
		}else{*/
			//获取用户角色权限
			Integer roleId = userLogin.getInt("role_id");
			List<RbacRoleMenu> list1 = RbacRoleMenu.dao.find("select b.* from (select * from  g_rbac_role_menu where roleid=?)as a left join g_rbac_menu b on a.menuid=b.id where b.status=1 order by b.sort,a.id",roleId);
				setReturnCode(SUCCESS);
				setReturnContent(list1);
				renderJson(resp);
			/*for (RbacRoleMenu rbacRoleMenu : list1) {
				 if(map.get(rbacRoleMenu.getLong("id"))==null){
					 map.put(rbacRoleMenu.getLong("id"), rbacRoleMenu);
				 }
			}*/
			//获取用户分组权限
			/*List<RbacUserGroup> userGroup = RbacUserGroup.dao.find("select * from g_rbac_user_group where user_id =?",userLogin.getLong("id"));
				if(userGroup.size() != 0){
				String sql = "(";
				for(RbacUserGroup rbacUserGroup : userGroup){
					sql += rbacUserGroup.get("group_id")+",";
				}
				sql = sql.substring(0,sql.length()-1);
				sql+=")";
				List<RbacGroupMenu> list2 = RbacGroupMenu.dao.find("select b.* from (select * from  g_rbac_group_menu where groupid in "+sql+" and 1=1)as a left join g_rbac_menu b on a.menuid=b.id where b.status=1");
				for (RbacGroupMenu rbacGroupMenu : list2) {
					 if(map.get(rbacGroupMenu.getLong("id"))==null){
						 map.put(rbacGroupMenu.getLong("id"), rbacGroupMenu);
					 }
				}
			  }*/
			//获取用户个人权限
			/*List<RbacPersonMenu> list3 = RbacPersonMenu.dao.find("select b.* from (select * from  g_rbac_person_menu where personid = "+userLogin.getLong("id")+")as a left join g_rbac_menu b on a.menuid=b.id where b.status=1");	
			for (RbacPersonMenu rbacPersonMenu : list3) {
				if(map.get(rbacPersonMenu.getLong("id"))==null){
					 map.put(rbacPersonMenu.getLong("id"), rbacPersonMenu);
				 }
			}*/
		//}
		//System.out.println(map.toString());
		//List<?> dataList = Db.find("select * from g_rbac_menu where status=1 order by sort");
		
	}
/*	@Before(SaveActValidator.class)
	public void addAction() {
		System.out.println("到了");
		System.out.println(getPara("act.uri"));
		AcAct m = getModel(AcAct.class);
		if (m.save()) {
			renderText("success");
		} else {
			renderText("error");
		}
	}*/
	
	public void actList() {
		renderJson(Db.find("select * from g_ac_act where pid=0 order by sort"));
	}
	/**
	 * 质平上报左侧菜单
	 */
	public void hospital(){
		
	}
/*	@Clear
	public void sortable() {
		String sortString = getPara("sortString");
		int i = 0;
		List<MySort> sortArray = JSONArray.parseArray(sortString, MySort.class);
		for (MySort object : sortArray) {
			System.out.println("id:" + object.id + " 顺序：" + ++i);
			Db.update("update ac_act set sort=? where id=?", ++i, object.id);
			if (object.children != null) {
				for (Children r : object.children) {
					System.out.println(r.id);
//					Db.update("");
				}
			}
		}
		renderNull();
	}*/
	
}
