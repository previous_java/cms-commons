package com.gdatacloud.cms.controller.manage.rbac;


import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gdatacloud.cms.model.rbac.RbacPersonMenu;
import com.gdatacloud.cms.model.rbac.RbacRoleMenu;
import com.gdatacloud.cms.model.sys.SysLogs;
import com.gdatacloud.cms.model.user.UserLogin;
import com.gdatacloud.cms.util.weixin.wex.util.VarValid;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.kit.HashKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.xyz.util.IpUtil;
import com.xyz.weixin.sdk.kit.IpKit;
/**
 * 用户管理控制器
 * @author Administrator
 *
 */
public class UserController extends Controller{
	/**
	 * 进入用户管理页面
	 */
	public void index() {
		render("user.html");
	}
	/**
	 * 条件查询获取用户列表
	 */
	public void list() {
		UserLogin user = getModel(UserLogin.class,"user");
		String hospitalname=getPara("hospital_name");
		String hospital_category_code = getPara("hospital_category_code");
		String hospital_level_code = getPara("hospital_level_code");
		String city = getPara("city");
		String area = getPara("area");
		
		Page<?> page = null;
		page = UserLogin.dao.list(getParaToInt("curr", 1), 10, user,hospitalname
				, hospital_category_code, hospital_level_code, city, area);
		setReturnCode(SUCCESS);
		setReturnContent(page);
		renderJson(resp);
	}
	
	public void getCheckPending() {
		// ?user.role_id=2&curr=1&user.username=&hospitalname=&user.status=3
		String sql = "select count(id) count from g_user_login where role_id=2 and status=3"
				+ " and id in (select user_login_id from g_hospital_info where state=2)";
		UserLogin u = UserLogin.dao.findFirst(sql);
		setReturnContent(u);
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	/**
	 * 获取用户信息
	 */
	@Clear
	public void getInfo(){
		Long id = getParaToLong("id");
		UserLogin user = UserLogin.dao.findById(id);
		if(user != null){
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		setReturnContent(user);
		renderJson(resp);
	}
	/**
	 * 新增、修改用户信息
	 * @throws UnknownHostException 
	 */
	public void update() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		UserLogin user = getModel(UserLogin.class,"user");
		//新增
		if(!VarValid.isValidOfLong(user.getLong("id"))){
			if(VarValid.isValidOfString(user.getStr("username")) && VarValid.isValidOfString(user.getStr("name")) && VarValid.isValidOfString(user.getStr("email"))){
				user.set("password", HashKit.md5(HashKit.sha1("111111"))).set("create_time", new Date());
				if(user.save()){//创建用户基本信息
					setReturnCode(SUCCESS);
					/*	//写入系统日志
					SysLogs.dao.logs(SysLogs.TYPE_ADD,"新增用户信息", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());*/

				}else{
					setReturnCode(ERROR);
				}
			}
		}else{//更新
			if(VarValid.isValidOfString(user.getStr("username")) && VarValid.isValidOfString(user.getStr("name")) && VarValid.isValidOfString(user.getStr("email"))){
				user.set("update_time", new Date());
				UserLogin user2 = UserLogin.dao.findById(user.getLong("id"));//更新主表详细
				user.set("password", user2.getPassword()).set("update_time", new Date());
				if(user.update()){//更新用户基本信息
				setReturnCode(SUCCESS);
				/*	//写入系统日志
					SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"修改用户信息", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());
*/
				}else{
					setReturnCode(ERROR);
				}
			}
		}
		renderJson(resp); 
	}
	/**
	 * 删除用户信息
	 * @throws UnknownHostException 
	 */
	public void del() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		Long id = getParaToLong("id");
		UserLogin user = UserLogin.dao.findById(id);
		user.set("status", 2).set("update_time", new Date());
		
		if(user.update()){
			setReturnCode(SUCCESS);
			//写入系统日志
//			SysLogs.dao.addLog(userId, IpKit.getRealIp(getRequest()), "删除用户信息：" + userLogin.getId() + userLogin.getUsername());
		}else{
			setReturnCode(ERROR);
		}
		setReturnContent(user);
		renderJson(resp);
	}
	/**
	 * 密码重置
	 * @throws UnknownHostException 
	 */
	public void updatePassword() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		Long id = getParaToLong("id");
		UserLogin user = UserLogin.dao.findById(id);
		if(user != null){
			user.set("password", HashKit.md5(HashKit.sha1("111111"))).update();
		}
		setReturnCode(SUCCESS);
		//写入系统日志
		//SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"密码重置", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());

		setReturnContent(user);
		renderJson(resp);
	}
	/*————————————————————————————分界线————————————————————————————*/
	/**
	 * 获取菜单列表
	 */
	@Clear
	public void getMenuList() {
		// 这种应该是再定义一个拦截器，提示它登录
		Long userId = getSessionAttr("userId");
		System.out.println(userId);
		UserLogin userLogin = UserLogin.dao.findById(userId);
		//该用户权限集合
		List<?> dataList = new ArrayList<Object>();
		Map<Long, Object> map = new HashMap<Long, Object>();
		
		
		List<String> list = new ArrayList<String>();
		if(userLogin.getId()==1){//超级管理员
			 dataList = Db.find("select * from g_rbac_menu where status=1 order by sort");
			 	setReturnCode(SUCCESS);
				setReturnContent(dataList);
				renderJson(resp);
				return;
		}else{
			//获取用户角色权限
			Integer role_id = userLogin.getInt("role_id");
			List<RbacRoleMenu> list1 = RbacRoleMenu.dao.find("select b.* from (select * from  g_rbac_role_menu where roleid=?)as a left join g_rbac_menu b on a.menuid=b.id where b.status=1",role_id);
			for (RbacRoleMenu rbacRoleMenu : list1) {
				 if(map.get(rbacRoleMenu.getLong("id"))==null){
					 map.put(rbacRoleMenu.getLong("id"), rbacRoleMenu);
				 }
			}
			/*//获取用户分组权限
			List<RbacUserGroup> userGroup = RbacUserGroup.dao.find("select * from g_rbac_user_group where user_id =?",userLogin.getLong("id"));
				if(userGroup.size() != 0){
				String sql = "(";
				for(RbacUserGroup rbacUserGroup : userGroup){
					sql += rbacUserGroup.get("group_id")+",";
				}
				sql = sql.substring(0,sql.length()-1);
				sql+=")";
				List<RbacGroupMenu> list2 = RbacGroupMenu.dao.find("select b.* from (select * from  g_rbac_group_menu where groupid in "+sql+" and 1=1)as a left join g_rbac_menu b on a.menuid=b.id where b.status=1");
				for (RbacGroupMenu rbacGroupMenu : list2) {
					 if(map.get(rbacGroupMenu.getLong("id"))==null){
						 map.put(rbacGroupMenu.getLong("id"), rbacGroupMenu);
					 }
				}
			  }*/
			//获取用户个人权限
			List<RbacPersonMenu> list3 = RbacPersonMenu.dao.find("select b.* from (select * from  g_rbac_person_menu where personid = "+userLogin.getLong("id")+")as a left join g_rbac_menu b on a.menuid=b.id where b.status=1");	
			for (RbacPersonMenu rbacPersonMenu : list3) {
				if(map.get(rbacPersonMenu.getLong("id"))==null){
					 map.put(rbacPersonMenu.getLong("id"), rbacPersonMenu);
				 }
			}
		}
		//System.out.println(map.toString());
		//List<?> dataList = Db.find("select * from g_rbac_menu where status=1 order by sort");
		setReturnCode(SUCCESS);
		setReturnContent(map);
		renderJson(resp);
	}
	/**
	 * 前往修改密码页面
	 */
	@Clear
	public void toUpadatePass(){
		render("/WEB-INF/content/rbac/updatePass.html");
	}
	/**
	 * 修改密码
	 * @throws UnknownHostException 
	 */
	@Clear
	public void updatePass() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		String  oldPass,pass,rePass;
		oldPass=getPara("oldPass");
		oldPass=HashKit.md5(HashKit.sha1(getPara("oldPass")));//旧密码，接收并加密
		
		pass= getPara("pass");
		rePass= getPara("rePass");
		
		if(pass.equals(rePass)){
			pass=HashKit.md5(HashKit.sha1(pass)); //新密码，接收并加密
			
			if(userLogin!=null && userLogin.getPassword().equals(oldPass)){
				userLogin.setPassword(pass);
				userLogin.update();
				getSession().invalidate();
				setReturnCode(SUCCESS);
				renderJson(resp);
				//写入系统日志
				//SysLogs.dao.logs(SysLogs.TYPE_UPDATE,"修改密码", userLogin.getRoleId(), userLogin.getStr("username"), userLogin.getLong("id"), IpUtil.getLocalIp());
				return;
			}else{
				renderText("noCorrect");
				setReturnCode(ERROR);
				setReturnContent("原密码输入有误！");
				renderJson(resp);
				return ;
			}
		}else{
			setReturnCode(ERROR);
			setReturnContent("确认密码与新密码不一致！");
			renderJson(resp);
			return;
		}
	}
	/**
	 * 获取权限列表
	 */
	public void getRole(){
		setReturnCode(SUCCESS);
		setReturnContent(UserLogin.dao.getRole());
		renderJson(resp);
	}
	/**
	 * 修改用户审核状态
	 */
	public void modifyStatus() {
		Long userId = getSessionAttr("userId");
//		UserLogin userLogin = UserLogin.dao.findById(userId);
		Long id = getParaToLong("id");
		String status = getPara("status");
		String remark = getPara("remark");
		
		UserLogin user = UserLogin.dao.findById(id);
		user.set("status", status).set("update_time", new Date());
		user.setImg(remark);
		if(user.update()){
			setReturnCode(SUCCESS);
			//写入系统日志
//			SysLogs.dao.addLog(userId, IpKit.getRealIp(getRequest()), "删除用户信息：" + userLogin.getId() + userLogin.getUsername());
		}else{
			setReturnCode(ERROR);
		}
		setReturnContent(user);
		renderJson(resp);
	}
}
