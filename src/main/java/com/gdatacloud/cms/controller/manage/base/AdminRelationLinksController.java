package com.gdatacloud.cms.controller.manage.base;

import com.gdatacloud.cms.model.relation.RelationLinks;
import com.jfinal.core.Controller;

public class AdminRelationLinksController extends Controller{
	/**
	 * 跳转参数设置首页
	 */
	public void index(){
		render("relationLink.html");
	}
	
	/**
	 * 显示所有友情链接
	 */
	public void AllRelationLinks(){
		setReturnCode(SUCCESS);
		setReturnContent(RelationLinks.dao.getAllRelationLinks());
		renderJson(resp);
	}
	/**
	 * 删除友情链接
	 */
	public void deleteRelationLinks(){
		if(RelationLinks.dao.deleteRelationLinks(getParaToInt("id"))){
			setReturnCode(SUCCESS);
			setReturnContent("删除成功");
		}else{
			setReturnCode(ERROR);
			setReturnContent("删除失败");
		}
		renderJson(resp);
	}
	/**
	 * 增加修改我访问友情链接
	 */
	public void updateOrSaveRelationLinks(){
		RelationLinks relationLinks=getModel(RelationLinks.class,"link");
		if(RelationLinks.dao.updateOrSaveRelationLinks(relationLinks)){
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	/**
	 * 根据id获取友情链接
	 */
	public void getRelationLinksById(){
		setReturnCode(SUCCESS);
		setReturnContent(RelationLinks.dao.getRelationLinksById(getParaToInt("id")));
		renderJson(resp);
	}
}
