package com.gdatacloud.cms.controller.manage.dict;

import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 获取区域控制器
 * @author zzp
 *
 */
public class RegionController extends Controller {

	/**
	 * 根据pid获取下一级节点
	 */
	public void getRegionByPid() {
		// 获取区域父节点id
		Integer pId = getParaToInt("pId", 0);
		List<Record> dataList = Db.find("select * from g_config_region where parentid=?", pId);
		setReturnCode(SUCCESS);
		setReturnContent(dataList);
		renderJson(resp);
	}
}
