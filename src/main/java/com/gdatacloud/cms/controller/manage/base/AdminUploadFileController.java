package com.gdatacloud.cms.controller.manage.base;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URLDecoder;

import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;

/**
 * 上传控制器
 * @author changkang
 *
 */
@Clear
public class AdminUploadFileController extends Controller{

	
	/**
	 * 断点上传附件
	 * 
	 */
	
	public void appendUpload(){
		String filePath = getRequest().getRealPath("/upload")+"/attachment";
		//如果文件夹不存在则创建    
	     File articlePath = new File(filePath);  
		if(!articlePath .exists()  && !articlePath .isDirectory()){       
			articlePath.mkdirs();    
	       }  
	
        try {
        	getRequest().setCharacterEncoding("utf-8");
           
            String fileSize = getRequest().getParameter("fileSize");
            long totalSize = Long.parseLong(fileSize);
            RandomAccessFile randomAccessfile = null;
            long currentFileLength = 0;// 记录当前文件大小，用于判断文件是否上传完成
            String currentFilePath = getRequest().getRealPath("/upload")+"/attachment/";// 记录当前文件的绝对路径
            String fileName = new String(getRequest().getParameter("fileName").getBytes("ISO-8859-1"),"UTF-8");
            fileName=URLDecoder.decode(fileName,"UTF-8");
            String lastModifyTime = getRequest().getParameter("lastModifyTime");
            File file = new File(currentFilePath+fileName+"."+lastModifyTime);
            // 存在文件
            if(file.exists()){
                randomAccessfile = new RandomAccessFile(file, "rw");
            }else {
                // 不存在文件，根据文件标识创建文件
            		
                randomAccessfile = new RandomAccessFile(currentFilePath+fileName+"."+lastModifyTime, "rw");
            }
                    // 开始文件传输
                InputStream in = getRequest().getInputStream();
                randomAccessfile.seek(randomAccessfile.length());
                byte b[] = new byte[1024];
                int n;
                while ((n = in.read(b)) != -1) {
                    randomAccessfile.write(b, 0, n);
                }

            currentFileLength = randomAccessfile.length();

            // 关闭文件
            closeRandomAccessFile(randomAccessfile);
            randomAccessfile = null;
            // 整个文件上传完成后,修改文件后缀
            if (currentFileLength == totalSize) {
           	
                        long  currentime = System.currentTimeMillis();
                        String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
                        String save_name_qianzhui=String.valueOf(currentime);
                        resp.put("src2", URLDecoder.decode(fileName,"UTF-8"));
            			String save_name = save_name_qianzhui+save_name_houzhui;
            			file.renameTo(new File(currentFilePath+save_name)); 

                       resp.put("src", "upload/attachment/"+save_name);
                       
                       setReturnCode(SUCCESS);
            }else{
            			resp.put("src", "");
            			 resp.put("src2", "");
            			setReturnCode(ERROR);
            }
            resp.put("currentFileLength", currentFileLength+"");
            renderJson(resp);
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}
	/**
	 * 获取上传附件大小
	 * 
	 */
	
	public void getFileSize(){
		String filePath = getRequest().getRealPath("/upload")+"/attachment";
		//如果文件夹不存在则创建    
	     File articlePath = new File(filePath);  
		if(!articlePath .exists()  && !articlePath .isDirectory()){       
			articlePath.mkdirs();    
	       }  
        //存储文件的路径，根据自己实际确定
        String currentFilePath = getRequest().getRealPath("/upload")+"/attachment/";
        try {
        	getRequest().setCharacterEncoding("utf-8");
            
            String fileName = new String(getRequest().getParameter("fileName").getBytes("ISO-8859-1"),"UTF-8");
           
            String lastModifyTime = getRequest().getParameter("lastModifyTime");
            File file = new File(currentFilePath+fileName+"."+lastModifyTime);
            if(file.exists()){         
                renderText(file.length()+"");
            }else{
                renderText(-1+"");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}
	
	
    /**
     * 关闭随机访问文件
     * 
     * @param randomAccessfile
     */
    public static void closeRandomAccessFile(RandomAccessFile rfile) {
        if (null != rfile) {
            try {
                rfile.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
	 * 上传文件
	 */
	public void getFileUpload() {
		// 获得文件上传保存路径
		long  currentime = System.currentTimeMillis();
		
		UploadFile file1 = getFile("image_files"); // 上传文件，将文件存入指定目录
		UploadFile file2 = getFile("voice_files"); // 上传文件，将文件存入指定目录
		UploadFile file3 = getFile("video_files");	       
		if (file1!=null) {
			String filePath = getRequest().getRealPath("/upload")+"/image/course";
			//如果文件夹不存在则创建    
		     File articlePath = new File(filePath);  
			if(!articlePath .exists()  && !articlePath .isDirectory()){       
				articlePath.mkdirs();    
		       }  
			System.out.println(1);
			String realpath = getRequest().getRealPath("/upload")+"/image/course";
			File file = file1.getFile();
			String fileName = file1.getFileName();
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}else if (file2!=null) {
			String realpath = getRequest().getRealPath("/upload")+"/audio/course";
			File file = file2.getFile();
			String fileName = file2.getFileName();
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}else if (file3!=null) {
			String realpath = getRequest().getRealPath("/upload")+"/video/course";
			File file = file3.getFile();
			String fileName = file3.getFileName();
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}
		//setReturnCode(SUCCESS);//测试
		renderJson(resp);
	}
}
