package com.gdatacloud.cms.controller.manage.base;

import java.util.List;

import org.apache.activemq.kaha.impl.index.Index;

import com.gdatacloud.cms.model.config.ConfigOption;
import com.jfinal.core.Controller;

public class AdminConfigOption extends Controller{
	/**
	 * 跳转参数设置首页
	 */
	public void index(){
		renderFreeMarker("configOptin.html");
	}
	
	/**
	 * 显示全部系统参数
	 * @return
	 */
	public void getOptionList(){
		setReturnCode(SUCCESS);
		setReturnContent(ConfigOption.dao.getOptionList(getParaToInt("pageNumber",1),getPara("content")));
		renderJson(resp);
	}
	
	/**
	 * 删除系统参数
	 */
	public void deleteOption(){
		if(ConfigOption.dao.deleteOption(getParaToLong("id"))){
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	
	/**
	 * 根据id查找系统参数
	 */
	public void getOptionById(){
		setReturnCode(SUCCESS);
		setReturnContent(ConfigOption.dao.getOptionById(getParaToLong("id")));
		renderJson(resp);
	}
	
	/**
	 * 添加或者修改系统参数
	 */
	public void updateOption(){
		ConfigOption option=getModel(ConfigOption.class, "option");
		option.setStatus(1);
		if(ConfigOption.dao.updateOption(option)){
			setReturnCode(SUCCESS);
			setReturnContent(option);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
}
