
package com.gdatacloud.cms.controller.manage.base;

import java.util.List;

import com.gdatacloud.cms.model.common.CommonColumn;
import com.gdatacloud.cms.model.models.ModelsInfo;
import com.jfinal.core.Controller;
/**
 * @author maoning
 * @version 0.0.1-SNAPSHOT 2016-12-31
 * @see 
 * @since  CMS 0.0.1-SNAPSHOT
 * 路由：column
 */
public class AdminColumnController extends Controller{

	public void index(){}

	public void page() {
		Long columnId= getParaToLong("id");
		if(getParaToLong("id") == null){
			setReturnCode(ERROR);
		}else{
			CommonColumn column = CommonColumn.dao.findById(columnId);
			if(column.getType() == 1){	//列表式
				render("module/article.html");
				return;
			}else if(column.getType() == 2){	//详情式
				render("module/detail_article.html");
				return;
			}else if(column.getType() == 3){	//下载式
				render("module/download_article.html");
				return;
			}else{
				setReturnCode(ERROR);
			}
			
		}
		renderJson(resp);
		//renderFreeMarker("module/article.html");
	}
	/**
	 *功能： 前往栏目管理
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void column(){
		setAttr("t", getPara("t"));
		renderFreeMarker("module/column.html");
	}
	
	/**
	 * 获取树型表格
	 */
	public void getFatherTree(){
		
	}
	/**
	 *功能： 获取栏目信息
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getcolumn(){
		String name = getPara("name");
		String queryFrom = "from g_common_column where type != 0";
		if (getPara("t") != null) {
			queryFrom += " and is_header='" + getPara("t") + "'"; 
		}
		if (name != null && !("".equals(name))) {
			queryFrom += " and name like '%" + name + "%' ";
		}
		queryFrom += " and enabled=1 ";
//		Page<CommonColumn> commonColumnList = CommonColumn.dao.paginate(getParaToInt("pageNumber"), 9, "select *", queryFrom);
		List<?> commonColumnList = CommonColumn.dao.find("select * " + queryFrom);
		setReturnCode(SUCCESS);
		setReturnContent(commonColumnList);
		renderJson(resp);
	}
	
	/**
	 *功能： 获取所有父节点
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getfatNode(){
		String querySql = "select * from g_common_column where 1=1 ";
		String isFinal = getPara("isFinal");
		if (isFinal != null) {
			 querySql += "and is_final='" + isFinal + "'";
		}
		querySql += "and enabled=1";
		List<CommonColumn> commonColumnList = CommonColumn.dao.find(querySql);
		renderJson(commonColumnList);
	}

	/**
	 *功能： 修改或添加栏目
	 *简介：获取id,若存在,则修改,不存在,删除
	 *		根据所选父栏目parent_id,若为0,则次栏目无父栏目,
	 *		若不为0,查询父栏目parent_ids,并在后追加所选父栏目id,并存入当前栏目parent_ids中.
	 *@since 0.0.1-SNAPSHOT
	 */
	public void addOrupdateColumn(){
		
		CommonColumn columnModel= getModel(CommonColumn.class,"column");
		
		if (getParaToInt("acId") == null) {
			
			int parent_id = columnModel.getInt("parent_id");
			String parentIds = null;
			if(parent_id == 0){

			}else{
				
				CommonColumn column =CommonColumn.dao.findById(columnModel.getInt("parent_id"));	//获取父栏目信息
				 parentIds = column.getStr("parent_ids");				
				if (parentIds ==null || parentIds.length() <0) {										//若不存在,说明父栏目为parent_id为0
					
					parentIds=String.valueOf(columnModel.getInt("parent_id"));
				}else{
					
					parentIds+=","+columnModel.getInt("parent_id");
				}//判断parentIds ==null || parentIds.length() <0的结束语
			}//判断parent_id == 0的结束语
			columnModel.set("parent_ids", parentIds);
			columnModel.setTplPath("modules/articles/page.html");
			columnModel.setAction("admin/column/page");
			columnModel.setType(1);
			
			columnModel.save();
		}else{
			
			int id = getParaToInt("acId");
			int parent_id = columnModel.getInt("parent_id");
			
			if (parent_id == 0) {

			}else{
				
				CommonColumn column =CommonColumn.dao.findById(columnModel.getInt("parent_id"));//获取父栏目信息
				String parentIds = column.getStr("parent_ids");
				if (parentIds ==null || parentIds.length() <0) {
					
					parentIds=String.valueOf(columnModel.getInt("parent_id"));					//若不存在,说明父栏目为parent_id为0
				}else{
					
					parentIds+=","+columnModel.getInt("parent_id");
				}//判断parentIds ==null || parentIds.length() <0的结束语
				columnModel.set("parent_ids", parentIds);
			}//判断parent_id == 0的结束语  
			columnModel.set("id", id);
			columnModel.setTplPath("modules/articles/page.html");
			columnModel.setAction("admin/column/page");
			columnModel.setType(1);
			
			columnModel.update();
			setReturnContent(columnModel);
		}//判断getParaToInt("acId") == null 的结束语
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	
	/**
	 *功能： 获取栏目详情
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getcolumByid(){
		int id = getParaToInt("acId");
//		CommonColumn column =CommonColumn.dao.findById(id);
//		column = CommonColumn.dao.findFirst("select a.name as pName, b.* from g_common_column a, g_common_column b where b.id=? and b.parent_id=a.id", id);
		CommonColumn column = CommonColumn.dao.findFirst("select a.*,b.name as pName from g_common_column a left join g_common_column b on a.parent_id=b.id where a.id=?", id);
		setReturnCode(SUCCESS);
		setReturnContent(column);
		renderJson(resp);
	}
	
	/**
	 *功能： 删除栏目
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void delColumn(){
		int  id = getParaToInt("acId");
		CommonColumn.dao.deleteById(id);
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	/**
	 * 获取模板
	 */
	public void getModel(){
		int modelType = getParaToInt("modelType");	//1 栏目 2  文章
		/*Long node = getParaToLong("node");	//node为0  父节点  其余 子节点
		int  ultimateColumn = getParaToInt("ultimateColumn"); //是否是终极栏目  1.是  0 不是
		if(modelType == 1){		//栏目
			
		}else if(modelType ==2){	//文章
			
		}else{
			
		}*/
		if(getParaToInt("modelType") !=null){
			List<ModelsInfo> modelsInfos = ModelsInfo.dao.getInfoByType(modelType);
			setReturnCode(SUCCESS);
			setReturnContent(modelsInfos);
		}
		renderJson(resp);
	}
}
