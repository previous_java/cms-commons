package com.gdatacloud.cms.controller.manage.base;

import java.io.File;
import java.net.UnknownHostException;
import java.util.List;

import com.gdatacloud.cms.interceptor.GlobalActionInterceptor;
import com.gdatacloud.cms.interceptor.LoginInterceptor;
import com.gdatacloud.cms.model.rbac.RbacRole;
import com.gdatacloud.cms.model.rbac.RbacRoleMenu;
import com.gdatacloud.cms.model.sys.SysLogs;
import com.gdatacloud.cms.model.user.UserLogin;
import com.gdatacloud.cms.util.RoleConst;
import com.gdatacloud.cms.util.weixin.wex.util.VarValid;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.kit.HashKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.upload.UploadFile;

/**
 * 后台管理的首页
 * @author zzp
 *
 */
@Before(LoginInterceptor.class)
public class AdminController extends Controller {

	/**
	 * 后台首页
	 */
	@Clear
	public void index() {
		// 这个地方判断，如果是已经登录，那么可以进入后台管理；否则跳转到登录页面
		Long userId = getSessionAttr("userId");
		if (userId == null) {
			
			render("/WEB-INF/content/login.html");
		} else {
			UserLogin login = UserLogin.dao.findById(userId);
			setAttr("user", login);
			if(login.getRoleId()==2){
				Record records=Db.findFirst("select * from g_hospital_info where user_login_id=?",userId);
				if(records==null||(records.getInt("state")==5)){
					redirect("/column/page?c=92");
				}else{
					setAttr("hospitalInfo", records);
					render("index.html");
				}
			}else{
				render("index.html");
			}
		}
	}
	
	/**
	 * 如果是get请求则返回页面，如果是post请求则验证登录
	 */
	@Clear
	public void login() {
		if ("GET".equals(getRequest().getMethod())) {
			render("/WEB-INF/content/login.html");
		} else {
			String username = getPara("username");
			String password = HashKit.md5(HashKit.sha1(getPara("password")));
			UserLogin userLogin = UserLogin.dao.findFirst("select id,role_id,status from g_user_login where username=? and password=?", username, password);
			if (userLogin != null) {
				int status = userLogin.getStatus();	
				resp.put("status", status);
				if(status == 1){		//使用
					setReturnCode(0);
					setSessionAttr("userId", (Long)userLogin.getId());
					CacheKit.put("ac", "userId", userLogin.getId());
					CacheKit.put("ac", "roleId", userLogin.getRoleId());
					//SysLogs.addLog((Long)userLogin.getId(), GlobalActionInterceptor.getIpAddress(getRequest()), "登陆");
				}else if(status == 2){	//删除
					setReturnCode(2);
					setReturnContent("账号已被删除");
					renderJson(resp);
					return;
				}else if(status == 3){	//未审核
						setReturnCode(3);
						setSessionAttr("userId", (Long)userLogin.getId());
						setSessionAttr("index", 1);
						CacheKit.put("ac", "userId", userLogin.getId());
						CacheKit.put("ac", "roleId", userLogin.getRoleId());
				}else if(status == -1){	//已驳回
					setReturnCode(4);
					setReturnContent("此账号未通过审核，请联系管理员");
					renderJson(resp);
					return;
				}else if(status == 0){	//已驳回
					setSessionAttr("userId", (Long)userLogin.getId());
				}
				
				if(userLogin.getRoleId()==2){
					List<Record> records=Db.find("select * from g_hospital_info where user_login_id=? and state!=4",(Long)userLogin.getId());
					setReturnCode(SUCCESS);
					setReturnContent(records);	
				}else{
					setReturnCode(SUCCESS);
					setReturnContent(false);
				}
			
			} else {
				setReturnContent("用户名或密码错误");
			}
			renderJson(resp);
		}
	}
	/**
	 * 判断在账号是否填写医院信息
	 */
	@Clear
	public void existHospital(){
		if(getSessionAttr("userId")!=null){
			Long userId=getSessionAttr("userId");
			List<Record> records=Db.find("select * from g_hospital_info where user_login_id=?",userId);
			if(records.isEmpty()){
				setReturnContent(0);
			}else{
				setReturnContent(1);
			}
		}else{
			setReturnContent(2);
		}
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	/**
	 * 控制台
	 */
	public void console () {
		Long userId = getSessionAttr("userId");
		if (!VarValid.isValidOfLong(userId)) {			
			redirect("/admin/login");
			return;
		}
		UserLogin user = UserLogin.dao.findById(userId);
		RbacRole role = RbacRole.dao.findById(user.getRoleId());
		setAttr("user", user);
		setAttr("role", role);
		switch (user.getRoleId()) {
		//管理员
		case RoleConst.ROLE_ADMIN:							//超级管理员
			render("/WEB-INF/content/manage/console/adminconsole.html");
			break;
		case RoleConst.ROLE_HOSPITAL_ADMIN:					//医院管理员
			render("/WEB-INF/content/manage/console/console.html");
			break;
		case RoleConst.ROLE_EXPERT:							//专家
			render("/WEB-INF/content/manage/console/adminconsole.html");
			break;
		default:
			setReturnCode(PRIVILEGE); // 如果没有，说明无权限
			render("/WEB-INF/content/manage/console/console.html");
			
			break;
		}
	}
	/**
	 * 获取快捷菜单
	 */
	public void getShortcutKey(){
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		Integer roleId = userLogin.getInt("role_id");
		List<Record> list1 = Db.find("select b.id,b.pid,b.name,b.uri,img_path from (select * from  g_rbac_role_menu where roleid=?)as a left join g_rbac_menu b on a.menuid=b.id where b.status=1 and b.index_status = 1",roleId);
		RbacRole role = RbacRole.dao.findFirst("select id from g_rbac_role_menu where roleid =? and menuid =?",roleId,2);
		List<Record> columns =null;
		if(role != null){
			columns = Db.find("select id,parent_id as pid,name,action as uri,img_path from g_common_column where index_status = 1");	
		}
		
		/*for (Record record : list1) {
			if(record.getLong("id") == 2){
				
			}
		}*/
		if(columns.size()>0){
			for (Record record : columns) {
				list1.add(record);
			}
		}
			setReturnCode(SUCCESS);
			setReturnContent(list1);
			renderJson(resp);
	}
	
	public void getUserMenuList(){
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		Integer roleId = userLogin.getInt("role_id");
		List<Record> list1 = Db.find("select b.id,b.pid,b.name,b.uri,img_path from (select * from  g_rbac_role_menu where roleid=?)as a left join g_rbac_menu b on a.menuid=b.id where b.status=1",roleId);
		RbacRole role = RbacRole.dao.findFirst("select id from g_rbac_role_menu where roleid =? and menuid =?",roleId,2);
		List<Record> columns =null;
		if(role != null){
			columns = Db.find("select id,parent_id as pid,name,action as uri,img_path from g_common_column");	
		}
		if(columns.size()>0){
			for (Record record : columns) {
				if(record.getInt("pid")== 0){
					record.set("pid", 2);
				}
				list1.add(record);
			}
		}
			setReturnCode(SUCCESS);
			setReturnContent(list1);
			renderJson(resp);
	}
	public void addShortcutKey() throws UnknownHostException{
		Long userId = getSessionAttr("userId");
		UserLogin userLogin = UserLogin.dao.findById(userId);
		int roleid = userLogin.getRoleId();
 		Long menuid = getParaToLong("menuid");
 		RbacRoleMenu rbacRoleMenu = RbacRoleMenu.dao.findFirst("select * from g_rbac_menu where roleid=? and menuid=?",roleid,menuid);
 		if(rbacRoleMenu == null){//勾选操作
 			
 		}else{//取消勾选操作
 			
 		}
		renderJson(resp);
	}
	/**
	 * 获取用户信息
	 */
	public void getuserInfo(){
		Long userId = getSessionAttr("userId");
		if(userId == null){
			setReturnCode(ERROR);
			renderJson(resp);
			return;
		}else{
			UserLogin login= UserLogin.dao.findById(userId);
			setReturnCode(SUCCESS);
			setReturnContent(login);
			renderJson(resp);

		}	
	}
	/**
	 * 修改用户信息
	 */
	public void updateUserInfo(){
		Long userId =getSessionAttr("userId");
		if(userId != null){
			UserLogin login = getModel(UserLogin.class,"user");
			login.setId(userId);
			login.update();
			//SysLogs.addLog(userId, GlobalActionInterceptor.getIpAddress(getRequest()), "修改id="+login.getId()+"的信息");
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	public void updatePass(){
		Long userId =getSessionAttr("userId");
		
		if(userId != null){
			String oldPass = HashKit.md5(HashKit.sha1(getPara("oldPassword")));
			String newPass = HashKit.md5(HashKit.sha1(getPara("newPassword")));
			String rePass = HashKit.md5(HashKit.sha1(getPara("rePassword")));
			UserLogin login = UserLogin.dao.findById(userId);
			if(login.getPassword().equals(oldPass)){
				if(newPass.equals(rePass)){
					login.setPassword(newPass);
					login.update();
				//	SysLogs.addLog(userId, GlobalActionInterceptor.getIpAddress(getRequest()), "修改id="+login.getId()+"的密码");
					setReturnCode(SUCCESS);
				}else{
					setReturnCode(LOGIN);	//两次输入密码不一致
				}
				
			}else{
				setReturnCode(PRIVILEGE);	//密码不正确
			}
			
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	
	public void updateImg(){
		// 获得文件上传保存路径
		Long userId = getSessionAttr("userId");
		if(userId == null){
			setReturnCode(ERROR);
			renderJson(resp);
			return;
		}else{
		long  currentime = System.currentTimeMillis();
		UploadFile file1 = getFile("image_files"); // 上传文件，将文件存入指定目录
		if (file1!=null) {
			String filePath = getRequest().getRealPath("/upload")+"/header";
			//如果文件夹不存在则创建    
		     File articlePath = new File(filePath);  
			if(!articlePath .exists()  && !articlePath .isDirectory()){       
				articlePath.mkdirs();    
		       }  
			System.out.println(1);
			String realpath = getRequest().getRealPath("/upload")+"/header";
			File file = file1.getFile();
			String fileName = file1.getFileName();
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			UserLogin login  = UserLogin.dao.findById(userId);
			login.setImg("upload/header/"+save_name);
			login.update();
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}
		renderJson(resp);
		}
	}
	/**
	 * 验证手手机号是否唯一
	 */
	public void phoneUniquely() {         
		long uId = getSessionAttr("userId");
		String phone = getPara("phone");
		UserLogin userLogin = UserLogin.dao.findFirst("select * from g_user_login where phone='"+phone+"' and id !="+uId);
		if(userLogin != null)
			setReturnCode(ERROR);
		else
			setReturnCode(SUCCESS);
		renderJson(resp);
	}
	
	/**
	 * 验证邮箱唯一性
	 */
	public void mailUniquely() {  
		long uId = getSessionAttr("userId");
		String mail = getPara("mail");
		UserLogin userLogin = UserLogin.dao.findFirst("select * from g_user_login where email='"+mail+"' and id !="+uId);
		if(userLogin != null)
			setReturnCode(ERROR);
		else
			setReturnCode(SUCCESS);
		renderJson(resp);
	}

	
	/**
	 * 退出
	 */
	@Clear
	public void logout(){
		getSession().invalidate();		
		setReturnCode(SUCCESS);
		renderJson(resp);
	}	
}
