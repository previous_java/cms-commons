package com.gdatacloud.cms.controller.manage.base;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gdatacloud.cms.model.articles.ArticlesFile;
import com.gdatacloud.cms.model.articles.Articles;
import com.gdatacloud.cms.model.common.CommonColumn;
import com.gdatacloud.cms.model.info.InfoDownload;
import com.gdatacloud.cms.model.user.UserLogin;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.mchange.v2.util.DoubleWeakHashMap;
/**
 * @author maoning
 * @version 0.0.1-SNAPSHOT 2017-01-01
 * @see 
 * @since  CMS 0.0.1-SNAPSHOT
 * 路由：article
 */
public class AdminArticleController extends Controller{

	public void index(){
		
	}
	/**
	 * 跳转到轮播图页面
	 */
	public void slidePicture(){
		render("module/slidePicture.html");
	}
	public void articleContent(){
		setAttr("id", getPara("id"));
		
	}
	/**
	 *功能： 获取文章列表
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getarticle(){
		String querySql = "from g_articles as a,g_common_column as c where a.column_id=c.id and a.status < 3";
		String name = getPara("name");
		Integer columnId = getParaToInt("articleColumn");
		if (name != null && !("".equals(name))) {
			querySql += " and a.title like '%" + name + "%' ";
		}
		if (columnId != null) {
			querySql += " and a.column_id=" + columnId;
		}
		querySql += " order by a.sort,a.id asc ";
		Page<?> articles = Db.paginate(getParaToInt("pageNumber"), 30, "SELECT a.id,a.title,a.create_time,c.name, a.sort,c.article_img_width, c.article_img_height", querySql);
		setReturnCode(SUCCESS);
		setReturnContent(articles);
		renderJson(resp);
	}
	
	/**
	 *功能： 修改或添加文章
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void addOrupdateArticle(){
		String filePath = getPara("filePaths");
		String filePaths[] = filePath.split(",");
		String fileName = getPara("fileNames");
		String fileNames[] = fileName.split(",");
		String fileSize = getPara("fileSize");
		String fileSizes[] = fileSize.split(","); 
		Articles articleModel= getModel(Articles.class,"article");
		String editorValue = getPara("editorValue");
		Integer status = getParaToInt("article.status");
		if (getParaToInt("acId") == null) {
			Long userId = getSessionAttr("userId");
			UserLogin login = UserLogin.dao.findById(userId);
			articleModel.set("main_body", editorValue);
			articleModel.set("create_time",new Date());
			articleModel.set("status", status);
			articleModel.setAuthorId(userId);
			articleModel.setAuthor(login.getName());
			articleModel.save();
		}else{
			int id = getParaToInt("acId");
			articleModel.set("id", id);
			articleModel.set("main_body", editorValue);
			articleModel.set("status", status);
			articleModel.update();
			Record article = Db.findFirst("SELECT a.id,a.title,a.create_time,c.name ,a.sort from g_articles as a,g_common_column as c where a.column_id=c.id and a.id=?",id);
			setReturnContent(article);
			ArticlesFile.dao.delFiles(id);
		}
		if(filePath != ""){
			for (int i = 0; i < filePaths.length; i++) {
				if(filePaths[i] != null){
					ArticlesFile artclesFile = new ArticlesFile();
					artclesFile.set("articleId", articleModel.get("id"))
							   .set("filePath", filePaths[i])
							   .set("fileName", fileNames[i])
							   .set("fileSize", fileSizes[i])
							   .save();
				}
			}
		}
		
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	
	
	/**
	 *功能： 获取文章详情
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getArticleByid(){
		// 接收id
		int id = getParaToInt("acId");
		// 获取详情
		Articles article =Articles.dao.findById(id);
		CommonColumn column = CommonColumn.dao.findFirst("select * from g_common_column where id=(select column_id from g_articles where id = ?)", id);
		List<ArticlesFile> files = ArticlesFile.dao.getFiles(id);
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("article", article);
		map.put("file", files);
//		map.put("pName", column.get("name"));
		setReturnCode(SUCCESS);
		setReturnContent(map);
		renderJson(resp);
	}
	/**
	 *功能： 删除文章
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void delArticle(){
		int  id = getParaToInt("acId");
		Articles article = Articles.dao.findById(id);
		article.setStatus(3);
		article.update();
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	
	/**
	 *功能： 初始化复选框
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void initUeditor(){

		renderJsp("jsp/controller.jsp");
	}
	
	/**
	 *功能： 上传封面图片
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getFileUpload() {
		// 获得文件上传保存路径
		long  currentime = System.currentTimeMillis();

		UploadFile file1 = getFile("image_files"); // 上传文件，将文件存入指定目录
		UploadFile file2 = getFile("voice_files"); // 上传文件，将文件存入指定目录
		UploadFile file3 = getFile("video_files");
		
		if (file1!=null) {
			System.out.println(1);
			String realpath = getRequest().getRealPath("/upload")+"/image/article";
			File real=new File(realpath);
			if(!real.exists()){
				real.mkdirs();
			}
			File file = file1.getFile();
			String fileName = file1.getFileName();
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}else if (file2!=null) {
			String realpath = getRequest().getRealPath("/upload")+"/audio/article";
			File file = file2.getFile();
			String fileName = file2.getFileName();
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}else if (file3!=null) {
			String realpath = getRequest().getRealPath("/upload")+"/video/article";
			File file = file3.getFile();
			String fileName = file3.getFileName();
			String save_name_houzhui = fileName.substring(fileName.lastIndexOf("."),fileName.length());
			String save_name = String.valueOf(currentime)+save_name_houzhui;
			file.renameTo(new File(realpath+"/"+save_name));
			
			setReturnCode(SUCCESS);
			setReturnContent(save_name);
		}
		//setReturnCode(SUCCESS);//测试
		renderJson(resp);
	}
	
	public void download(){
		String path = getPara("path");
		if(path != null){
			File file=new File(getRequest().getRealPath("/")+path);
			renderFile(file);
		}
	}
	/**
	 * 获取此文章下的所有文件列表
	 */
	public void getFileList(){
		int id = getParaToInt("articleId");
		List<ArticlesFile> articlesFiles = ArticlesFile.dao.getFiles(id);
		setReturnCode(SUCCESS);
		setReturnContent(articlesFiles);
		renderJson(resp);
	}
	public  void delFile(){
		Long fileId = getParaToLong("fileId");
		ArticlesFile.dao.deleteById(fileId);
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	public void updateFileName(){
		Long fileId = getParaToLong("fileId");
		if(fileId != null){
			ArticlesFile articlesFile = ArticlesFile.dao.findById(fileId);
			String filename = getPara("fileName");
			if(articlesFile != null && getPara("fileName")!= null){
				
				articlesFile.set("fileName", filename);
				articlesFile.update();
				setReturnCode(SUCCESS);
			}else{
				setReturnCode(PRIVILEGE);
			}
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	/*
	 * 获取文章详情内容
	 */
	public void detailArticle(){
		Long columnId= getParaToLong("id");
		if(columnId != null){
			CommonColumn column = CommonColumn.dao.findById(columnId);
			setReturnCode(SUCCESS);
			setReturnContent(column);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	/**
	 * 更新文章详情页
	 */
	public void UpdateDetailArticle(){
		CommonColumn column = getModel(CommonColumn.class,"column");
		column.update();
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	/**
	 * 获取下载列表
	 */
	public void downLoadList(){
		String name = getPara("name");
		Long columnId = getParaToLong("id");
		Page<InfoDownload> infoList = InfoDownload.dao.infoList(getParaToInt("page", 1), 10, name, columnId);
		setReturnCode(SUCCESS);
		setReturnContent(infoList);
		renderJson(resp);
	}
	/**
	 * 更新或添加下载信息
	 */
	public void addOrUpdateDownLoad(){
		InfoDownload download = getModel(InfoDownload.class,"info");
		Long id = getParaToLong("id");
		if(id == null){
			download.setStatus(1);
			download.save();
		}else{
			download.set("id", id);
			download.setUpdateTime(new Date());
			download.update();
		}
		setReturnCode(SUCCESS);
		setReturnContent(download);
		renderJson(resp);
	}
	/**
	 * 删除下载信息
	 */
	public void delDownload(){
		Long id  = getParaToLong("id");
		InfoDownload download = InfoDownload.dao.findById(id);
		download.setStatus(2);
		download.update();
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	/**
	 * 获取下载信息
	 */
	public void getDownLoad(){
		Long id  = getParaToLong("id");
		InfoDownload download = InfoDownload.dao.findById(id);
		setReturnCode(SUCCESS);
		setReturnContent(download);
		renderJson(resp);
	}
	/**
	 * 用户投稿
	 */
	public void contribute(){
		render("module/contribute.html");
	}
	/**
	 * 管理员审稿
	 */
	
	public void adminContribute(){
		render("module/adminContribute.html");
	}
	/**
	 *功能： 获取文章列表
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getUserArticle(){
		Long userId = getSessionAttr("userId");
		String querySql = "from g_articles where 1=1 and author_id = "+userId+" and status != 3";
		String name = getPara("name");
		//Integer columnId = 39;
		if (name != null && !("".equals(name))) {
			querySql += " and title like '%" + name + "%' ";
		}
		/*if (columnId != null) {
			querySql += " and a.column_id=" + columnId;
		}*/
		querySql += " order by sort ,id asc";
		Page<?> articles = Db.paginate(getParaToInt("pageNumber"), 30, "SELECT id,title,create_time,status,sort", querySql);
		setReturnCode(SUCCESS);
		setReturnContent(articles);
		renderJson(resp);
	}
	
	/**
	 *功能： 获取文章详情
	 *简介：
	 *@since 0.0.1-SNAPSHOT
	 */
	public void getUserArticleByid(){
		// 接收id
		int id = getParaToInt("acId");
		// 获取详情
		Articles article =Articles.dao.findById(id);
		List<ArticlesFile> files = ArticlesFile.dao.getFiles(id);
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("article", article);
		map.put("file", files);
		setReturnCode(SUCCESS);
		setReturnContent(map);
		renderJson(resp);
	}
	/**
	 * 管理员获取用户投稿列表
	 */
	public void getUserArticleList(){
		String querySql = "from g_articles where status = 1 or status=5";
		String name = getPara("name");
		//Integer columnId = 39;
		if (name != null && !("".equals(name))) {
			querySql += " and title like '%" + name + "%' ";
		}
		/*if (columnId != null) {
			querySql += " and a.column_id=" + columnId;
		}*/
		querySql += " order by sort,id article";
		Page<?> articles = Db.paginate(getParaToInt("pageNumber"), 30, "SELECT id,title,create_time,status,author,sort", querySql);
		setReturnCode(SUCCESS);
		setReturnContent(articles);
		renderJson(resp);
	}
	/**
	 * 发布用户投稿
	 */
	public void releaseUserArticle(){
		Long acId = getParaToLong("acId");
		Articles article = Articles.dao.findById(acId);
		article.setStatus(2);
		article.update();
		setReturnCode(SUCCESS);
		renderJson(resp);
	}
	/**
	 * 向上移动
	 */
	public void moveUp() {
		// 接收文章id，查询当前文章的前一条
		Integer articleId = getParaToInt("articleId");
		Articles thisArticle = Articles.dao.findById(articleId);
		// 取出前一篇文章的id和排序标识
		Articles anotherArticle = Articles.dao.findFirst("select id,sort"
				+ " from g_articles where sort<? and column_id=? order by sort desc limit 1", thisArticle.getSort(), thisArticle.getColumnId());
		if (anotherArticle == null) {
			setReturnCode(3001);
		} else {
			// 将当前文章的排序标识和此文章的排序标识互换
			Db.update("update g_articles set sort=" + anotherArticle.getSort() +" where id=" + articleId);
			Db.update("update g_articles set sort=" + thisArticle.getSort() + " where id=" + anotherArticle.getId());
			setReturnCode(SUCCESS);
		}
		renderJson(resp);
	}
	public void moveDown() {
		// 接收文章id，查询当前文章的前一条
		Integer articleId = getParaToInt("articleId");
		Articles thisArticle = Articles.dao.findById(articleId);
		// 取出前一篇文章的id和排序标识
		Articles anotherArticle = Articles.dao.findFirst("select id,sort"
				+ " from g_articles where sort>? and column_id=? order by sort limit 1", thisArticle.getSort(), thisArticle.getColumnId());
		if (anotherArticle == null) {
			setReturnCode(3001);
		} else {
			// 将当前文章的排序标识和此文章的排序标识互换
			Db.update("update g_articles set sort=" + anotherArticle.getSort() +" where id=" + articleId);
			Db.update("update g_articles set sort=" + thisArticle.getSort() + " where id=" + anotherArticle.getId());
			setReturnCode(SUCCESS);
		}
		renderJson(resp);
	}
}
