package com.gdatacloud.cms.controller.manage.base;

import com.gdatacloud.cms.model.config.ConfigDictionary;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;

public class AdminDictionaryController extends Controller{
	/**
	 * 跳转参数设置首页
	 */
	public void index(){
		renderFreeMarker("dictionary.html");
	}
	/**
	 * 显示全部系统参数
	 * @return
	 */
	public void getDictionaryList(){
		setReturnCode(SUCCESS);
		setReturnContent(ConfigDictionary.dao.getDictionaryList(getParaToInt("pageNumber",1),getPara("content"),getPara("typeId")));
		renderJson(resp);
	}
	/**
	 * 添加一级数据字典
	 */
	public void addCode(){
		ConfigDictionary dictionary=getModel(ConfigDictionary.class,"dictionary");
		if(dictionary.getParentCode()==null){
			dictionary.setParentCode(0);
		}else{
			ConfigDictionary dictionary2=ConfigDictionary.dao.getConfigDictionary(dictionary.getParentCode());
			dictionary.setTypeId(dictionary2.getTypeId());
		}
		dictionary.setStatus(1);
		if(ConfigDictionary.dao.save(dictionary)){
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	/**
	 * 获取菜单分类
	 */
	public void getType(){
		setReturnCode(SUCCESS);
		setReturnContent(ConfigDictionary.dao.getType());
		renderJson(resp);
	}
	/**
	 * 目前只删除一级数据字典
	 */
	public void deleteDictionary(){
		int code=getParaToInt("code");
		if(ConfigDictionary.dao.deleteDictionary(code)){
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	/**
	 * 根据主键获取一条记录
	 */
	public void getDictionary(){
		int code=getParaToInt("code");
		setReturnCode(SUCCESS);
		setReturnContent(ConfigDictionary.dao.getConfigDictionary(code));
		renderJson(resp);
	}
	/**
	 * 修改字典表
	 */
	public void updateDictionary(){
		int code=getParaToInt("code");
		String content=getPara("dictionary.content");
		String sort=getPara("dictionary.sort");
		if(ConfigDictionary.dao.updateDictionary(sort,content,code)){
			setReturnCode(SUCCESS);
		}else{
			setReturnCode(ERROR);
		}
		renderJson(resp);
	}
	/**
	 * 获取二级菜单
	 */
	public void getSonDictionaryList(){
		setReturnCode(SUCCESS);
		setReturnContent(ConfigDictionary.dao.getSonDictionaryList(getParaToInt("pageNumber",1),getParaToInt("code")));
		renderJson(resp);
	}
	/**
	 * 返回二级菜单页面
	 */
	public void sonDictionaryHtml(){
		render("dictionary_2.html");
	}
	
	/**
	 * 向上移动
	 */
	public void moveUp() {
		// 接收文章id，查询当前文章的前一条
		Integer dicId = getParaToInt("dicId");
		ConfigDictionary thisDic = ConfigDictionary.dao.findFirst("select * from g_config_dictionary where code=?", dicId);
		// 取出前一篇文章的id和排序标识
		ConfigDictionary anotherDic = ConfigDictionary.dao.findFirst("select code,sort"
				+ " from g_config_dictionary where sort<? and type_id=? and parent_code=? order by sort desc limit 1", thisDic.getSort(), thisDic.getTypeId(), thisDic.getParentCode());
		if (anotherDic == null) {
			setReturnCode(3001);
		} else {
			// 将当前文章的排序标识和此文章的排序标识互换
			Db.update("update g_config_dictionary set sort=" + anotherDic.getSort() +" where code=" + dicId);
			Db.update("update g_config_dictionary set sort=" + thisDic.getSort() + " where code=" + anotherDic.getCode());
			setReturnCode(SUCCESS);
		}
		renderJson(resp);
	}
	public void moveDown() {
		// 接收文章id，查询当前文章的前一条
		Integer dicId = getParaToInt("dicId");
		ConfigDictionary thisDic = ConfigDictionary.dao.findFirst("select * from g_config_dictionary where code=?", dicId);
		// 取出前一篇文章的id和排序标识
		ConfigDictionary anotherDic = ConfigDictionary.dao.findFirst("select code,sort"
				+ " from g_config_dictionary where sort>? and type_id=? and parent_code=? order by sort limit 1", thisDic.getSort(), thisDic.getTypeId(), thisDic.getParentCode());
		if (anotherDic == null) {
			setReturnCode(3001);
		} else {
			// 将当前文章的排序标识和此文章的排序标识互换
			Db.update("update g_config_dictionary set sort=" + anotherDic.getSort() +" where code=" + dicId);
			Db.update("update g_config_dictionary set sort=" + thisDic.getSort() + " where code=" + anotherDic.getCode());
			setReturnCode(SUCCESS);
		}
		renderJson(resp);
	}
}
