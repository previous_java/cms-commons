package com.gdatacloud.cms.model.articles;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

public class ArticlesFile extends Model<ArticlesFile>{
	public static final ArticlesFile dao = new ArticlesFile();
	
	public List<ArticlesFile> getFiles(int id){
		return ArticlesFile.dao.find("select * from g_articles_file where articleId=?",id);
	}
	
	public void delFiles(int id){
		Db.update("delete from g_articles_file where articleId=?",id);
	}
}
