package com.gdatacloud.cms.model.common.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCommonDynamicSubject<M extends BaseCommonDynamicSubject<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setModule(java.lang.String module) {
		set("module", module);
	}

	public java.lang.String getModule() {
		return get("module");
	}

	public void setAttr(java.lang.String attr) {
		set("attr", attr);
	}

	public java.lang.String getAttr() {
		return get("attr");
	}

	public void setExceptSelect(java.lang.String exceptSelect) {
		set("except_select", exceptSelect);
	}

	public java.lang.String getExceptSelect() {
		return get("except_select");
	}

	public void setName(java.lang.String name) {
		set("name", name);
	}

	public java.lang.String getName() {
		return get("name");
	}

}
