package com.gdatacloud.cms.model.weixin;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("g_weixin_automatic_response", "id", WeixinAutomaticResponse.class);
		arp.addMapping("g_weixin_menu", "id", WeixinMenu.class);
		arp.addMapping("g_weixin_public_info", "id", WeixinPublicInfo.class);
	}
}

