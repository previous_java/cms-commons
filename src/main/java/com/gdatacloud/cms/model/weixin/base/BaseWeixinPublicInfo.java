package com.gdatacloud.cms.model.weixin.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseWeixinPublicInfo<M extends BaseWeixinPublicInfo<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}

	public java.lang.Long getId() {
		return get("id");
	}

	public void setOrigionId(java.lang.String origionId) {
		set("origion_id", origionId);
	}

	public java.lang.String getOrigionId() {
		return get("origion_id");
	}

	public void setAppId(java.lang.String appId) {
		set("app_id", appId);
	}

	public java.lang.String getAppId() {
		return get("app_id");
	}

	public void setAppSecret(java.lang.String appSecret) {
		set("app_secret", appSecret);
	}

	public java.lang.String getAppSecret() {
		return get("app_secret");
	}

	public void setName(java.lang.String name) {
		set("name", name);
	}

	public java.lang.String getName() {
		return get("name");
	}

	public void setSubscribeTips(java.lang.String subscribeTips) {
		set("subscribe_tips", subscribeTips);
	}

	public java.lang.String getSubscribeTips() {
		return get("subscribe_tips");
	}

	public void setDomainName(java.lang.String domainName) {
		set("domain_name", domainName);
	}

	public java.lang.String getDomainName() {
		return get("domain_name");
	}

	public void setToken(java.lang.String token) {
		set("token", token);
	}

	public java.lang.String getToken() {
		return get("token");
	}

	public void setPartner(java.lang.String partner) {
		set("partner", partner);
	}

	public java.lang.String getPartner() {
		return get("partner");
	}

	public void setPartnerKey(java.lang.String partnerKey) {
		set("partner_key", partnerKey);
	}

	public java.lang.String getPartnerKey() {
		return get("partner_key");
	}

	public void setImg(java.lang.String img) {
		set("img", img);
	}

	public java.lang.String getImg() {
		return get("img");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}

	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}

	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
