package com.gdatacloud.cms.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import com.jfinal.kit.PropKit;

//import com.sun.mail.util.MailSSLSocketFactory;

/**
 * @author 常康
 */

public class SendEmail{
	//收件人邮箱地址】
	private String to;
	//发件人邮箱地址
	private String from=PropKit.use("debug.txt").get("username");
	//SMTP服务器地址
	private String smtpServer;
	//登录SMTP服务器的用户名
	private String username=PropKit.use("debug.txt").get("username");
	//登录SMTP服务器的密码
	private String password=PropKit.use("debug.txt").get("emailpassword");
	//邮件主题
	private String subject;
	//邮件正文
	private String content;
	//记录所有附件文件的集合
	List<String> attachments=new ArrayList<String>();
	//有参构造器
	public SendEmail(String to, String from, String smtpServer, String username,String password, String subject, String content) {
		this.to = to;
		this.from = from;
		this.smtpServer = smtpServer;
		this.username = username;
		this.password = password;
		this.subject = subject;
		this.content = content;
	}
	//无参构造器
	public SendEmail() {}
	
	
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSmtpServer() {
		return smtpServer;
	}
	public void setSmtpServer(String smtpServer) {
		this.smtpServer = smtpServer;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	//把邮件主题转换成中文
	public String transferChinese(String strText){
		try {
			strText=MimeUtility.encodeText(new String(strText.getBytes()),"GB2312","B");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return strText;
	}
	
	//增加附件，将附件文件名添加到List集合中
	public void attachfile(String fname){
		attachments.add(fname);
	}
	
	//发送邮件
	public boolean send(){
		//创建邮件session所需的properties对象
		Properties props=new Properties();
		//开启ssl加密
		//MailSSLSocketFactory sf;
		
			//sf = new MailSSLSocketFactory(); 
			//sf.setTrustAllHosts(true);
			props.put("mail.smtp.ssl.enable", "true");
			//props.put("mail.smtp.ssl.socketFactory", sf);
			props.put("mail.smtp.host", smtpServer);
			props.put("mail.smtp.auth", "true");
	

	
		//创建session对象
		Session session=Session.getDefaultInstance(props
				//以匿名内部类的形式创建登录服务器的认证对象
				,new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication(){
				return new PasswordAuthentication(username, password);
			}
		});
		
		try {
			//构造MimeMessage并设置相关属性值
			MimeMessage msg=new MimeMessage(session);
			//设置发件人
			try {
				msg.setFrom(new InternetAddress(from,"浙江省质控平台"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//设置收件人
			InternetAddress addresses[]={new InternetAddress(to)};
			msg.setRecipients(Message.RecipientType.TO, addresses);
			//设置邮件主题
			subject=transferChinese(subject);
			msg.setSubject(subject);
			//构造Multipart
			Multipart mp=new MimeMultipart();
			//向Mulipart添加正文
			MimeBodyPart mbpContent=new MimeBodyPart();
			mbpContent.setText(content);
			//将BodyPart添加到Multipart容器中
			mp.addBodyPart(mbpContent);
			//向Multipart添加附件
			//遍历附件列表，将所有文件添加到邮件消息里
			for (String efile : attachments) {
				MimeBodyPart mbpFile=new MimeBodyPart();
				//以文件名创建FileDataSource对象
				FileDataSource fds=new FileDataSource(efile);
				//处理附件
				mbpFile.setDataHandler(new DataHandler(fds));
				mbpFile.setFileName(fds.getName());
				//将BodyPart添加到Multipart容器中
				mp.addBodyPart(mbpFile);
			}
			//清空附件列表
			attachments.clear();
			//向Multipart添加MimeMessage
			msg.setContent(mp);
			//设置发送日期
			msg.setSentDate(new Date());
			//发送邮件
			Transport.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static String wangyiMail(String toEmail,String authUserId,String url){
		String returnCode = "error";
		SendEmail sendMail=new SendEmail();
		//设置SMTP服务器地址
		sendMail.setSmtpServer("smtp.163.com");
	/*	//此处设置登录的名称#
		sendMail.setUsername("13137128413@163.com");
		//此处设置登录密码
		sendMail.setPassword("c875079028");*/
		//设置收件人地址
		sendMail.setTo(toEmail);
		//设置发件人地址
	/*	sendMail.setFrom(sendMail.getUsername());*/
		//设置标题
		sendMail.setSubject("找回密码");
		//设置内容
		sendMail.setContent(url+"?authUserId="+authUserId);
		//添加附件，可以添加多个,填写附件路径，没有可以不写,
		//sendMail.attachfile("src/lee/SendMail.java");
		//sendMail.attachfile("build.xml");
		
		if(sendMail.send()){
			System.out.println("----------邮件发送成功！---------");
			returnCode = "success";
		}else{
			System.out.println("----------邮件发送失败！---------");
			returnCode = "error";
		}
		return returnCode;
		
	}
	public static String wangyiMail(String toEmail,String content){
		String returnCode = "error";
		SendEmail sendMail=new SendEmail();
		//设置SMTP服务器地址
		sendMail.setSmtpServer("smtp.163.com");
		/*//此处设置登录的名称
		sendMail.setUsername("13137128413@163.com");
		//此处设置登录密码
		sendMail.setPassword("c875079028");*/
		//设置收件人地址
		sendMail.setTo(toEmail);
		//设置发件人地址
		/*sendMail.setFrom(sendMail.getUsername());*/
		//设置标题
		sendMail.setSubject("浙江省临床放射质控中心注册验证码");
		//设置内容
		sendMail.setContent(content);
		//添加附件，可以添加多个,填写附件路径，没有可以不写,
		//sendMail.attachfile("src/lee/SendMail.java");
		//sendMail.attachfile("build.xml");
		
		if(sendMail.send()){
			System.out.println("----------邮件发送成功！---------");
			returnCode = "success";
		}else{
			System.out.println("----------邮件发送失败！---------");
			returnCode = "error";
		}
		return returnCode;
		
	}
	public static String wangyiMail1(String toEmail,String authUserId,String url){
		String returnCode = "error";
		SendEmail sendMail=new SendEmail();
		//设置SMTP服务器地址
		sendMail.setSmtpServer("smtp.163.com");
		/*//此处设置登录的名称
		sendMail.setUsername("13137128413@163.com");
		//此处设置登录密码
		sendMail.setPassword("c875079028");*/
		//设置收件人地址
		sendMail.setTo(toEmail);
		//设置发件人地址
		/*sendMail.setFrom(sendMail.getUsername());*/
		//设置标题
		sendMail.setSubject("找回密码");
		//设置内容
		sendMail.setContent(url+"?authUserId="+authUserId+"&email="+toEmail);
		//添加附件，可以添加多个,填写附件路径，没有可以不写,
		//sendMail.attachfile("src/lee/SendMail.java");
		//sendMail.attachfile("build.xml");
		
		if(sendMail.send()){
			System.out.println("----------邮件发送成功！---------");
			returnCode = "success";
		}else{
			System.out.println("----------邮件发送失败！---------");
			returnCode = "error";
		}
		return returnCode;
		
	}
	
	public static void main(String[] args) {
		SendEmail sendMail=new SendEmail();
		String returnString=sendMail.wangyiMail("zealpane@163.com", "Email 验证码验证\n验证码："
				+ "\n这封信是由 浙江省临床放射质控中心 发送的。\n您收到这封邮件，是由于在 浙江省临床放射质控中心 进行了新用户注册使用了这个邮箱地址。如果您并没有访问过 浙江省临床放射质控中心，或没有进行上述操作，请忽 略这封邮件。您不需要退订或进行其他进一步的操作。\n----------------------------------------------------------------------\n帐号激活说明\n----------------------------------------------------------------------\n\n如果您是 浙江省临床放射质控中心 的新用户，或在修改您的注册 Email 时使用了本地址，我们需 要对您的地址有效性进行验证以避免垃圾邮件或地址被滥用。\n您只需复制下面的验证码填写入注册页面中的验证码填写区域即可。：\n\n\n感谢您的访问，祝您使用愉快！\n此致\n浙江省临床放射质控中心 管理团队.");
		System.out.println(returnString);
	}
}

