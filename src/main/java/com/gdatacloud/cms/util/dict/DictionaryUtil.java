package com.gdatacloud.cms.util.dict;
/**
 * 字典表工具类
 * @author 常康
 *
 */
public class DictionaryUtil {
	/**
	 * 设备类型
	 */
	public static final int ITEM_CATEGORY=1;
	/**
	 * 医院类别
	 */
	public static final int HOSPITAL_CATEGORY=2;
	/**
	 * 医院等级
	 */
	public static final int HOSPITAL_LEVEL=3;
	/**
	 * 性别
	 */
	public static final int SEX=4;
	/**
	 * 岗位
	 */
	public static final int JOBS=5;
	/**
	 * 学历
	 */
	public static final int EDUCATION=6;
	/**
	 * 职称
	 */
	public static final int POSITION=7;
	/**
	 * 诊断报告优良率类别
	 */
	public static final int EXCELLENT_CATEGORY=8;
	/**
	 * 病理结果负荷率类型
	 */
	public static final int LOAD_CATEGORY=9;
	/**
	 * 学位
	 */
	public static final int DEGREE=10;
	/**
	 * 工作量类型
	 */
	public static final int WORK=11;
	/**
	 * 影像优良率类型
	 */
	public static final int IMG=12;
	/**
	 * 阳性率类型
	 */
	public static final int POSITIVE=13;
	/**
	 * 人员类别
	 */
	public static final int PERSON=14;
	/**
	 * 设备厂商
	 */
	public static final int VENDOR=15;
 
}