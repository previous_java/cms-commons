package com.gdatacloud.cms.util.dict;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.gdatacloud.cms.model.config.ConfigDictionary;
import com.gdatacloud.cms.model.config.ConfigDictionaryType;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.druid.DruidPlugin;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class AutoDictionary {
/**
 * 自动生成字典类
 * @author 常康
 */
	public static void main(String[] args) throws TemplateException, IOException, SQLException {
		String fileName = PathKit.getWebRootPath() + "/src/main/java/com/gdatacloud/cms/util/dict/DictionaryUtil.java";
		DruidPlugin dp = new DruidPlugin("jdbc:mysql://192.168.127.179:3306/cms_zhikong?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull", "zhikong", "cms-zhikong");
		ActiveRecordPlugin arp=new ActiveRecordPlugin(dp);
		dp.start();
		arp.start();
		List<Record> record=Db.find("select * from g_config_dictionary_type");
		arp.stop();
		dp.stop();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		for (int i = 0; i < record.size(); i++) {
			dataMap.put("dictionary", record); 
		}
	     taskBook("DictionaryUtil.ftl", fileName, dataMap);
System.out.println("----------执行完毕----------");
	}

	
	public static void taskBook(String templetName, String outputFileName, Map<String, Object> dataMap) throws TemplateException, IOException {

		Configuration configuration = new Configuration();
		configuration.setDefaultEncoding("utf-8");
		
		configuration.setClassForTemplateLoading(AutoDictionary.class, "/com/gdatacloud/cms/util/dict/");
		Template t=null;
		try {
			t = configuration.getTemplate(templetName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File outputFile = new File(outputFileName);

	    if (!outputFile.getParentFile().exists()) {
			outputFile.getParentFile().mkdirs(); // 如果文件夹不存在，则创建之
		}
	    
		Writer out = null;
		out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile),"UTF-8"));
		t.process(dataMap, out);
		out.flush();
		out.close();
	}
	

}
