package com.gdatacloud.cms.util.dict;
/**
 * 字典表工具类
 * @author 常康
 *
 */
public class DictionaryUtil {
<#list dictionary as dictionary>
	/**
	 * ${dictionary.des!}
	 */
	public static final int ${dictionary.code!}=${dictionary.id!};
</#list>
 
}