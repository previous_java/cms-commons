package com.gdatacloud.cms.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.jfinal.plugin.activerecord.Record;

public class ExcelUtil {

	/**
	 * 
	 * @param name  sheet 名称
	 * @param header	头部
	 * @param data	数据
	 */
	public static String createExcel(String name,String[]header, List<Record> data,String filePath){
		
		// 第一步，创建一个webbook，对应一个Excel文件  
        HSSFWorkbook wb = new HSSFWorkbook();  
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet  
        HSSFSheet sheet = wb.createSheet(name);  
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short  
        HSSFRow row = sheet.createRow((int) 0);  
        // 第四步，创建单元格，并设置值表头 设置表头居中  
        HSSFCellStyle style = wb.createCellStyle();  
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式  
        HSSFCell cell = null;
        for (int i = 0; i < header.length; i++) {
        	 cell = row.createCell((short) i);  
             cell.setCellValue(header[i]);  
             cell.setCellStyle(style);
		}
        if(data != null){
        	for (int i = 0; i < data.size(); i++) {
        		// 第五步，写入实体数据 实际应用中这些数据从数据库得到，  
        	
        	row = sheet.createRow((int) i + 1);
        
        	for (int j =0 ; j < header.length; j++) {
        		String value =null;
        		 value = data.get(i).get(header[j]).toString();
  				 row.createCell((short) j).setCellValue(value);
			}
		}
        }
        
     //   List list = CreateSimpleExcelToDisk.getStudent();  
  
       /* for (int i = 0; i < list.size(); i++)  
        {  
            row = sheet.createRow((int) i + 1); 
            Student stu = (Student) list.get(i);  
            // 第四步，创建单元格，并设置值  
            row.createCell((short) 0).setCellValue((double) stu.getId());  
            row.createCell((short) 1).setCellValue(stu.getName());  
            row.createCell((short) 2).setCellValue((double) stu.getAge());  
            cell = row.createCell((short) 3);  
            cell.setCellValue(new SimpleDateFormat("yyyy-mm-dd").format(stu  
                    .getBirth()));  
        }*/  
        // 第六步，将文件存到指定位置  
        long  currentime = System.currentTimeMillis();
        String filenames = filePath+"/"+currentime+".xls";
        File file = new File(filenames);
        try  
        {  
        	
            FileOutputStream fout = new FileOutputStream(file);  
            wb.write(fout);  
            fout.close();  
            
        }  
        catch (Exception e)  
        {  
            e.printStackTrace();  
        } 
        return currentime+".xls";
	}
	/**
	 * 
	 * @param name  sheet 名称
	 * @param header	头部
	 * @param data	数据
	 */
	public static void exportExcel(String name,String[]header, List<Record> data,HttpServletResponse response){
		
		// 第一步，创建一个webbook，对应一个Excel文件  
        HSSFWorkbook wb = new HSSFWorkbook();  
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet  
        HSSFSheet sheet = wb.createSheet(name);  
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short  
        HSSFRow row = sheet.createRow((int) 0);  
        // 第四步，创建单元格，并设置值表头 设置表头居中  
        HSSFCellStyle style = wb.createCellStyle();  
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式  
        HSSFCell cell = null;
        /*for (int i = 0; i < header.length; i++) {
        	 cell = row.createCell((short) i);  
             cell.setCellValue(header[i]);  
             cell.setCellStyle(style);
		}*/
        if(data != null){
			for (int i = 0; i < data.size(); i++) {
				// 第五步，写入实体数据 实际应用中这些数据从数据库得到，

				row = sheet.createRow((int) i + 1);

				for (int j = 0; j < header.length; j++) {
					String value = null;
					value = data.get(i).get(header[j]);
					row.createCell((short) j).setCellValue(value);
				}
			}
        }
        
        // 第六步，将文件存到指定位置  
        long  currentime = System.currentTimeMillis();
        try  
        {  
        	
        	response.setContentType("application/vnd.ms-excel");
        	response.setHeader("Content-Disposition", "attachment; filename="+currentime+".xls");
        	OutputStream outputStream = response.getOutputStream();
            wb.write(outputStream);  
            outputStream.close();  
            
        }  
        catch (Exception e)  
        {  
            e.printStackTrace();  
        }
	}
}
