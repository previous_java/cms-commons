package com.gdatacloud.cms.util;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.PathKit;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class WriteTxt {
	
	
	public static boolean writeFile(String rootPath,String file,String content){
		
		
		String fileName = PathKit.getWebRootPath() + "/src/main/java/com/gdatacloud/cms/util/dict/page.html";
		
		Map<String, Object> dataMap = new HashMap<String, Object>();

			dataMap.put("content", content); 
	
	   
			taskBook("page.ftl", file, dataMap,rootPath);
	     System.out.println("----------执行完毕----------");
		return true;
		
	}
	

	
	public static void taskBook(String templetName, String outputFileName, Map<String, Object> dataMap,String rootPath){

		Configuration configuration = new Configuration();
		configuration.setDefaultEncoding("utf-8");
		configuration.setClassForTemplateLoading(WriteTxt.class, "/com/gdatacloud/cms/util/dict/");
		Template t=null;
		try {
			t = configuration.getTemplate(templetName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File outputFile = new File(outputFileName);

	    if (!outputFile.getParentFile().exists()) {
			outputFile.getParentFile().mkdirs(); // 如果文件夹不存在，则创建之
		}
	    
		Writer out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile),"UTF-8"));
			t.process(dataMap, out);
			out.flush();
			out.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (TemplateException  e) {
			// TODO: handle exception
			e.printStackTrace();
		}catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
}
