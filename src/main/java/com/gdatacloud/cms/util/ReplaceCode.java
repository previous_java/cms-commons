package com.gdatacloud.cms.util;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class ReplaceCode {

/**
 * 根据字典表中的code转化为中文
 */
	public static String execute(int typeId,int code){
		Record record= Db.findFirst("select * from g_config_dictionary where type_id=? and code=?",typeId,code);
		return record==null ? "" : record.getStr("content");
	}
/**
 * 中文转化为code
 */
	public static Integer execute(String content,int typeId){
		Record record= Db.findFirst("select * from g_config_dictionary where content=? and type_id=?",content,typeId);
		if(record==null){
			return null;
		}else{
			return record.getInt("code");
		}
	}
}
