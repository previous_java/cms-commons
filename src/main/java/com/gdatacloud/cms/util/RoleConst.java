package com.gdatacloud.cms.util;

public class RoleConst {

	/**
	 * 超级管理员
	 */
	public final static int ROLE_ADMIN = 1;
	
	/**
	 * 医院管理员
	 */
	public final static int ROLE_HOSPITAL_ADMIN = 2;
	
	/**
	 * 专家
	 */
	public final static int ROLE_EXPERT = 3; 
}
