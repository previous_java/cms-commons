/**
 * Copyright (c) 2015-2016, Michael Yang 杨福海 (fuhai999@gmail.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gdatacloud.cms.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.gdatacloud.cms.core.JMainConst;
import com.jfinal.core.Const;
import com.jfinal.kit.PathKit;
import com.jfinal.render.FreeMarkerRender;
import com.jfinal.render.RenderException;
import com.xyz.JMainTags;

import freemarker.template.Template;

public class JMStaticiseUtil {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void staticize(HttpServletRequest request) {

		Map<String, Object> jpTags = new HashMap<String, Object>();
		jpTags.putAll(JMainTags.jmTags);
		Map data = new HashMap();
		for (Enumeration<String> attrs = request.getAttributeNames(); attrs.hasMoreElements();) {
			String attrName = attrs.nextElement();
			if (attrName.startsWith("jm.")) {
				jpTags.put(attrName.substring(3), request.getAttribute(attrName));
			} else {
				data.put(attrName, request.getAttribute(attrName));
			}
		}
		data.put("jm", jpTags);

		try {
			getHtmlContent(data);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("rawtypes")
	private void getHtmlContent(Map data) throws FileNotFoundException {
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileOutputStream fos = new FileOutputStream(PathKit.getWebRootPath() + "/index.html");
		OutputStreamWriter osWriter = null;
		try {
			osWriter = new OutputStreamWriter(fos, Const.DEFAULT_ENCODING);
			Template template = FreeMarkerRender.getConfiguration().getTemplate(
					JMainConst.DEFAULT_TPL_PATH + "/modules/articles/list.html");
			template.process(data, osWriter);
			osWriter.flush();
		} catch (Exception e) {
			if (Const.DEFAULT_DEV_MODE) { // 如果是开发模式那么打印堆栈信息
				e.printStackTrace();
			}
			throw new RenderException(e);
		} finally {
			close(fos);
			close(osWriter);
		}
	}

	private void close(Writer writer) {
		if (writer != null) {
			try {
				writer.close();
			} catch (IOException e) {
			}
		}
	}

	private void close(OutputStream stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
			}
		}
	}

}
