package com.gdatacloud.cms.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadTxt {
	/**
     * 以行为单位读取文件，常用于读面向行的格式化文件
     * 读取数据库配置信息
     */
    public static Map<String, String> readFileByLines(String fileName) {
        Map<String, String> data=new HashMap<String, String>();
        
    	File file = new File(fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 1;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
            String lineData[]=tempString.split("= ");
            data.put(lineData[0], lineData[1]);
                line++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
		return data;
    }
    /**
     * 	读取列名
     * @param fileName
     * @return
     */
    public static String readFileGetRows(File file) {
        /*List<String> rowList = new ArrayList<String>();*/
        String content= "";
    	BufferedReader reader = null;
        try {
        	InputStreamReader streamReader = new InputStreamReader(new FileInputStream(file),"utf-8");
            reader = new BufferedReader(streamReader);
            String tempString = null;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
            //rowList.add(tempString+"\n");
            content +=tempString+"\n";
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
		return content;
    }
}
