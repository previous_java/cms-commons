package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
/**
 * 这个标签是用来
 * @author zzp
 *
 */
public class RecordListTag extends JTag {

	public static String tagName = "recordList";

	@Override
	public void onRender() {
		// 接收数据来源表的名称
		List<?> recordList = Db.find("select * from " + getParam("tableName"));
		setVariable(tagName, recordList);
		renderBody();
	}

}
