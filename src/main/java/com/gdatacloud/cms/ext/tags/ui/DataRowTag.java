package com.gdatacloud.cms.ext.tags.ui;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

public class DataRowTag extends JTag {

	public static String tagName = "dataRow";

	@Override
	public void onRender() {
		// 接收id，如果id不为空，那么按id取值；如果id为空，那么按其他条件查询单条记录
		Integer id = getParamToInt("id");
		Record dataRow;
		String tableName = getParam("table");
		if (id != null) {
			dataRow = Db.findById(tableName, id);
			if ("y".equals(getParam("update"))) {				
				Db.update("update " + tableName + " set visit_count=visit_count+1 where id=?", id);
			}
		} else {
			String querySql = "select ";
			// 接收所要查询的参数，如果为空那么赋值为*
			String getFields = getParam("fields");
			String fields = (getFields == null) ? "*" : getFields;
			querySql += fields;

			querySql += " from ";

			// 接收传过来的表名，没有表名不行
			if (tableName == null) {
				return;
			}
			querySql += tableName;
			
			// 接收where需要拼接的条件
			String where = getParam("where");
			if (where != null) {
				querySql += " where ";
				querySql += where;
			}
			// 排序
			String orderBy = getParam("orderBy");
			if (orderBy != null) {
				querySql += " order by ";
				querySql += orderBy;
			}
			dataRow = Db.findFirst(querySql);
			if ("y".equals(getParam("update"))) {				
				Db.update("update " + tableName + " set visit_count=visit_count+1 where " + where);
			}
		}
		setVariable("dataRow", dataRow);
		renderBody();
	}

}