package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class NavPathTag extends JTag {

	public static String tagName = "navPath";

	@Override
	public void onRender() {
		// 获取当前栏目的id，根据id到数据库中取栏目列表。栏目存到缓存中，然后这个地方从缓存中取值替换
		String columnIds = getParam("columnIds");
		if (columnIds == null) {
			return;
		}
		// 这地方随后要换成从缓存中取数据
		List<Record> columns = Db.find("select * from g_common_column where id in (" + columnIds + ")");
		setVariable("navs", columns);
		renderBody();
	}
}