package com.gdatacloud.cms.ext.tags.function;

import com.xyz.JFunction;

public class CssCurrentFunction extends JFunction {

	public static String functionName = "cssCurrent";
	
	@Override
	public Object onExec() {
		// 判断第一个参数和第二个参数是否相等，如果相等那么返回第三个参数，否则返回空
		String v1 = getToString(0);
		String v2 = getToString(1);
		String cssClass = getToString(2);
		// 如果v1不为空，那么比较
		if (v1 != null && !"".equals(v1)) {
			return v1.equals(v2) ? cssClass : "";
		}
		return null;
	}

	
}
