package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class ArticleTag extends JTag {

	public static String tagName = "article";

	@Override
	public void onRender() {
		// TODO Auto-generated method stub
		Record article = Db.findById("g_articles", getParamToInt("id"));
		setVariable("article", article);
		renderBody();
	}

}
