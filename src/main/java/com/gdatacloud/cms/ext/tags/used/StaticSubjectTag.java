package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class StaticSubjectTag extends JTag {

	public static String tagName = "staticSubject";

	@Override
	public void onRender() {
		// 传过来专题名称，然后根据专题取其下列表
		String subjectName = getParam("subject");
		// 判断是否分页，根据什么排序
		// TODO Auto-generated method stub
		Page<Record> articleList = Db.paginate(1, 1, "select * ", "from g_articles");
		setVariable(tagName, articleList);
		renderBody();
	}

}
