package com.gdatacloud.cms.ext.tags.used;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

public class ListChildrenColumnTag extends JTag {

	public static String tagName = "listChildrenColumn";

	@Override
	public void onRender() {
		// 获取当前栏目的id，根据id到数据库中取栏目列表。栏目存到缓存中，然后这个地方从缓存中取值替换
		String columnIds = getParam("columnId");
		if (columnIds == null) {
			return;
		}
		String querySql = "select * from g_articles";
		// 这地方随后要换成从缓存中取数据
		List<Record> columns = Db.find("select * from g_common_column where id in (" + columnIds + ")");
		setVariable("navs", columns);
		renderBody();
	}
}
