package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.ehcache.CacheKit;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class TaxonomyTag extends JTag {

	public static String tagName = "taxonomy";

	@Override
	public void onRender() {
		// TODO Auto-generated method stub
		String limit = getParam("limit");
		if(limit == null || limit == ""){
			List<?> taxonomy = Db.find("select * from g_common_column where enabled=1 and parent_id=?", getParamToInt("parentId"));
			setVariable(tagName, taxonomy);
		}else{
			List<?> taxonomy = Db.find("select * from g_common_column where enabled=1 and parent_id=? limit "+limit, getParamToInt("parentId"));
			setVariable(tagName, taxonomy);
		}

/*		CacheKit.put("ac", "column", taxonomy);
		List<Record> r = CacheKit.get("ac", "column");
		for (Record record : r) {
			System.out.println(record.getStr("name"));
		}
*/		renderBody();
	}

}
