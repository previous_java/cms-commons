package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class OptionTag extends JTag {

	public static String tagName = "option";

	@Override
	public void onRender() {
		// TODO Auto-generated method stub
		Record article = Db.findById("jpress_content", getParamToInt("id"));
		setVariable("article", article);
		renderBody();
	}

}
