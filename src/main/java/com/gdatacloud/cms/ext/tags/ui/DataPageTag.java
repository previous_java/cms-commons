package com.gdatacloud.cms.ext.tags.ui;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;
/**
 * 顶部导航标签
 * @author zzp
 * 调用方式：<@jm.dataList fields="字段名用半角逗号隔开" table="表名" where="where后面跟的查询条件" >
 * </@jm.dataList>
 */
public class DataPageTag extends JTag {

	public static String tagName = "dataPage";
	
	@Override
	public void onRender() {
		// 接收所要查询的参数，如果为空那么赋值为*
		String getFields = getParam("fields");
		String fields = (getFields == null) ? "*" : getFields;

		String sqlExceptSelect = " from ";

		// 接收传过来的表名，没有表名不行
		String tableName = getParam("table");
		if (tableName == null) {
			return;
		}
		sqlExceptSelect += tableName;
		
		// 接收where需要拼接的条件
		String where = getParam("where");
		if (where != null) {
			sqlExceptSelect += " where ";
			sqlExceptSelect += where;
		}
		// 排序
		String orderBy = getParam("orderBy");
		if (orderBy != null) {
			sqlExceptSelect += " order by ";
			sqlExceptSelect += orderBy;
		}
		Page<Record> dataPage = Db.paginate(getParamToInt("p", 1), getParamToInt("pageSize", 10)
				, "select " + fields, sqlExceptSelect);
		setVariable("dataPage", dataPage);
		renderBody();
	}

}
