package com.gdatacloud.cms.ext.tags.function;

import com.xyz.JFunction;

public class SubStrFunction extends JFunction {

	public static String functionName = "subStr";
	
	@Override
	public Object onExec() {
		// 第一个参数是字符串，第二个参数是所需字数
		String origionStr = getToString(0);
		int endIndex = getToInt(1, 1);
		if (endIndex < origionStr.length()) {			
			return origionStr.substring(0, (int)endIndex) + "...";
		}
		return origionStr;
	}

	
}
