package com.gdatacloud.cms.ext.tags.ui;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;
/**
 * 顶部导航标签
 * @author zzp
 * 调用方式：<@jm.dataList fields="字段名用半角逗号隔开" table="表名" where="where后面跟的查询条件" >
 * </@jm.dataList>
 */
public class DataListTag extends JTag {

	public static String tagName = "dataList";
	
	@Override
	public void onRender() {
		String querySql = "select ";
		// 接收所要查询的参数，如果为空那么赋值为*
		String getFields = getParam("fields");
		String fields = (getFields == null) ? "*" : getFields;
		querySql += fields;

		querySql += " from ";

		// 接收传过来的表名，没有表名不行
		String tableName = getParam("table");
		if (tableName == null) {
			return;
		}
		querySql += tableName;
		
		// 接收where需要拼接的条件
		String where = getParam("where");
		if (where != null) {
			querySql += " where ";
			querySql += where;
		}
		// 排序
		String orderBy = getParam("orderBy");
		if (orderBy != null) {
			querySql += " order by ";
			querySql += orderBy;
		}
		// 限制记录条数
		String limit = getParam("limit");
		if (limit != null) {
			querySql += " limit ";
			querySql += limit;
		}
		List<Record> dataList = Db.find(querySql);
		setVariable(tagName, dataList);
		renderBody();
	}

}
