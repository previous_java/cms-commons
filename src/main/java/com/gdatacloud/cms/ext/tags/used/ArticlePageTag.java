package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class ArticlePageTag extends JTag {

	public static String tagName = "articlePage";

	@Override
	public void onRender() {
		// TODO Auto-generated method stub
		Integer columnId = getParamToInt("columnId");
		Page<Record> articleList = Db.paginate(
				getParamToInt("pageNumber",1), 1, 
				"select * ", "from g_articles where column_id=?", columnId);
		setVariable(tagName, articleList);
		renderBody();
	}
}