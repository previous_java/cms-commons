package com.gdatacloud.cms.ext.tags.used;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.xyz.JTag;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
/**
 * 版块
 * @author zzp
 *
 */
public class SectionTag extends JTag {

	public static String tagName = "section";

	@Override
	public void onRender() {
		// 接收栏目
		//Integer columnId = getParamToInt("columnId");
		// 1、根据栏目，查询多少条记录，按什么排序。更多，那是按什么来显示呢
		//Integer listSize = getParamToInt("listSize", 5);
		// 接收排序字段
		
		
		String querySql = "select ";
		// 接收所要查询的参数，如果为空那么赋值为*
		String getFields = getParam("fields");
		String fields = (getFields == null) ? "*" : getFields;
		querySql += fields;

		querySql += " from ";

		// 接收传过来的表名，没有表名不行
		String tableName = getParam("table");
		if (tableName == null) {
			return;
		}
		querySql += tableName;
		
		// 接收where需要拼接的条件
		String where = getParam("where");
		if (where != null) {
			querySql += " where ";
			querySql += where;
		}
		String limit = getParam("limit");
		if (limit != null) {
			querySql += " limit ";
			querySql += limit;
		}
		List<Record> dataList = Db.find(querySql);
		setVariable("dataList", dataList);
		renderBody();
	}
}