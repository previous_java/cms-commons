package com.gdatacloud.cms.interceptor;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.gdatacloud.cms.model.sys.SysVisit;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.xyz.utils.Response;

public class GlobalActionInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation arg0) {
		HttpServletRequest request=arg0.getController().getRequest();
		String ip=getIpAddress(request);
		if(arg0.getController().getSession().isNew()){
			SysVisit visit=new SysVisit();
			visit.setIp(ip);
			visit.setCreatetime(new Date());
			visit.setTime(new Date());
			visit.save();
		}
		arg0.invoke();
		
	}
	  public static String getIpAddress(HttpServletRequest request) { 
		    String ip = request.getHeader("x-forwarded-for"); 
		    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		      ip = request.getHeader("Proxy-Client-IP"); 
		    } 
		    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		      ip = request.getHeader("WL-Proxy-Client-IP"); 
		    } 
		    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		      ip = request.getHeader("HTTP_CLIENT_IP"); 
		    } 
		    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		      ip = request.getHeader("HTTP_X_FORWARDED_FOR"); 
		    } 
		    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) { 
		      ip = request.getRemoteAddr(); 
		    } 
		    return ip; 
		  } 

}
