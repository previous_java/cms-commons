package com.gdatacloud.cms.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.xyz.utils.Response;

public class LoginInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		Long userId = controller.getSessionAttr("userId"); //获取用户id
		if (userId == null || userId == 0) {
			controller.setReturnCode(Response.LOGIN);
			controller.renderJson(controller.resp);
			return;
		}else {
			inv.invoke();
		} 
	}

}
