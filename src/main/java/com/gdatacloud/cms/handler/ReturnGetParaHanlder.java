package com.gdatacloud.cms.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

public class ReturnGetParaHanlder extends Handler {

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		// 获取所有的get参数并回传
		request.setAttribute("Req", request);
		request.setAttribute("Session", request.getSession());
		
		next.handle(target, request, response, isHandled);
	}

}
