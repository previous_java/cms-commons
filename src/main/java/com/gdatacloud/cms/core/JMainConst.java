package com.gdatacloud.cms.core;

import java.util.HashMap;
import java.util.Map;

/**
 * 定义基础常量
 * @author zzp
 *
 */
public class JMainConst {

	public static final String VERSION = "0.0.1";
	public static String DEFAULT_TPL = "/default";
	public static String BASE_TPL_PATH = "/WEB-INF/templates";
	public static String DEFAULT_TPL_PATH = "/WEB-INF/templates/zj";
	
	public static Map<Object, Object> data = new HashMap<Object, Object>();
	
}
